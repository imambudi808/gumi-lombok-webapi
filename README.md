<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## How To Install APP
- this app contain passport
- download composer
- clone : git@gitlab.com:imambudi808/gumi-lombok-webapi.git
- update composer : composer update
- and run app : php artisan serve


**[download android APP on play store ](https://play.google.com/store/apps/details?id=id.gumilombok)

[visit android app source code : ](https://gitlab.com/imambudi808/gumi-lombok-android)**
