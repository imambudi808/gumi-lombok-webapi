<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $dates = ['created_at'];
    protected $fillable = [
        'blog_id','blog_judul','blog_category','blog_creator','blog_creator_des','blog_image','blog_isi','created_at','updated_at','seo_link','total_view','total_comment','blog_is_hot'

    ];
    protected $primaryKey = 'blog_id';

}
