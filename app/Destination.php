<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $dates = ['created_at'];
    protected $fillable = [
        'destination_id','destination_name','destination_image','destination_content','destination_author','created_at','trash','destination_category','destination_lat','destination_long','destination_addres','rata_rating','jumlah_response','seo_link'
    ];
    protected $primaryKey = 'destination_id';

    public function islands(){
        return $this->hasOne('App\Island', 'island_id', 'island_id');
    }

    public function DestinationToRating(){
        return $this->hasMany('App\Rating','destination_id','destination_id');
    }

    public function Point(){
        return $this->hasMany('App\Point', 'destination_id');
    }
}
