<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DompetPoint extends Model
{
    protected $fillable = [
        'dompet_point_id','point_value','user_id','point_name','point_id','created_at','trash'
    ];

    public $timestamps = false;
}
