<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $evnt=['created_at'];
    protected $fillable = [
        'event_id','event_name','event_image','event_cost','event_location','event_date','created_at','updated_at','trash'
    ];
    protected $primaryKey ='event_id';
}
