<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TravelAgent;

class AgentTravelController extends Controller
{
    public function showAllAgentTravel(Request $request){
        $agentTravel = TravelAgent::offset($request->input('offset'))->limit($request->input('limit'))->get();
        $data = $agentTravel->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data 
        ],200);
    }
}
