<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Blog;
use Validator;
use Illuminate\Support\Facades\DB;

class BlogController extends Controller
{
    public function index(Request $request){
        $blog=Blog::offset($request->input('offset'))->limit($request->input('limit'))->orderby('created_at','DESC')->get();
        $data = $blog->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data tampil',
            'data' => $data
        ],200);
    }

    public function showHotMove(){
        $blog=Blog::where('blog_is_hot',1)->limit(6)->orderby('created_at','DESC')->get();
        $data = $blog->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data tampil',
            'data' => $data
        ],200);
    }

    public function showBlogByCategory(Request $request){
        if($request->input('blog_category')!=null){
            $blog=Blog::where('blog_category',$request->input('blog_category'))->offset($request->input('offset'))->limit($request->input('limit'))->orderby('created_at','DESC')->get();
        }else{
            $blog=Blog::offset($request->input('offset'))->limit($request->input('limit'))->orderby('created_at','DESC')->get();
        }
        
        $data = $blog->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data tampil',
            'data' => $data
        ],200);
    }

}
