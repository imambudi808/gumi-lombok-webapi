<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CovidController extends Controller
{
    public function covid19(){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://corona.ntbprov.go.id/",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
        echo $response;
        }
    }

    public function showCovidINA()
    {
        //https://covid19.mathdro.id/api/countries/ID/confirmed(DETAH SERVER)
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://api.kawalcorona.com/indonesia');
        $response = $request->getBody()->getContents();
        return response()->json([
            'status' => true,
            'pesan' => 'data covid 19 INA berhasil tampil',
            'data' => json_decode($response)      
            
        ],200);
    }

    public function showCovidIdProvince(){
        $client = new \GuzzleHttp\Client();
        $request = $client->get('https://api.kawalcorona.com/indonesia/provinsi');
        $response = $request->getBody()->getContents();
        return response()->json([
            'status' => true,
            'pesan' => 'data covid 19 INA berhasil tampil',
            'data' => json_decode($response)      
            
        ],200);
    }
}
