<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Destination;
use Validator;
use Illuminate\Support\Facades\DB;

class DestinationController extends Controller
{
    public function index(Request $request)
    {          

        $destination = Destination::with('islands')->offset($request->input('offset'))->limit($request->input('limit'))->orderby('created_at','DESC')->get();
        $data = $destination->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data destinasi berhasil ditampilkan',
            'data' => $data
        ], 200);
    }

    public function store(Request $request){
        $input = $request->all();

        $validator = Validator::make($input,[
            'destination_name' => 'required',
            'destination_image' => 'required',
            'destination_content' => 'required',
            'destination_author' => 'required'
        ]);
        if ($validator->fails()) {
            $response=[
                'success' => false,
                'data' => 'Validation Error',
                'message' => $validator->errors()
            ];
            return response()->json($response,404);
        }

        $destination = Destination::create($input);
        $data = $destination->toArray();

        $response=[
            'success' => true,
            'date' => $data,
            'message' => 'Data destinasi berhasil di tambahkan'
        ];
        return response()->json($response,200);
    }

    public function populerDestination(Request $request){

        
        $dompet_point = DompetPoint::with('islands')->where('user_id',$request->input('user_id'))->get();
        $data = $dompet_point->toArray();

        $var = $dompet_point;
        $total =0;
        foreach($var as $a){
            $total += $a->point_value;
        }
        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data,
            'total_point' => $total
        ], 200);
    }

    public function newDes(){

        $destination = Destination::with('islands')->orderby('created_at')->offset(0)->limit(5)->get();
        $data = $destination->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data destinasi berhasil ditampilkan',
            'data' => $data
        ], 200);        
    }

    public function updatededes(Request $request){
        $destination = Destination::where('destination_id',$request->input('destination_id'));

        $destination->update(['rata_rating'=> $request->input('rata_rating')]);
        $destination->update(['jumlah_response'=> $request->input('jumlah_response')]);
        // $data = $destination->toArray();
        return response()->json([
            'status' => true,
            'pesan' => 'data destinasi berhasil diperbaharui',
            'data' => [$request->all()]
        ], 200);  
    }
}
