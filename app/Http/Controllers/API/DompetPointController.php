<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DompetPoint;
use App\User;
use Validator;
use Carbon\Carbon;

class DompetPointController extends Controller
{
    public function show()
    {
        $dompet_point=DompetPoint::all();
        $data = $dompet_point->toArray();

        $var = DompetPoint::all();
        $total =0;
        foreach($var as $a){
            $total += $a->point_value;
        }

        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data,
            'total_point' => $total
            
        ],200);
    }

    public function showByUserId(Request $request){
        $dompet_point = DompetPoint::where('user_id',$request->input('user_id'))->get();
        $data = $dompet_point->toArray();

        $var = $dompet_point;
        $total =0;
        foreach($var as $a){
            $total += $a->point_value;
        }
        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data,
            'total_point' => $total
        ], 200);
    }

    public function insertPointToDompet(Request $request){
        $input = $request->all();
        $input['created_at'] = Carbon::now();
        $validator = Validator::make($input,[           
            'point_value' => 'required',
            'user_id' => 'required',
            'point_name' => 'required',
            'point_id' => 'required'
        ]);
        if ($validator->fails()) {
            $response=[
                'status' => false,
                'pesan' => $validator->errors(),
                'data' => 'semua data harus terisi'
            ];
            return response()->json($response,404);
        }
        // $dompet_point = DompetPoint::create($input);
        // $data = $dompet_point->toArray();

        $dompet_point = DompetPoint::all();

        $status = 1;
        foreach ($dompet_point as $dompet) {
            if ($dompet->user_id == $request->input('user_id') && $dompet->point_id == $request->input('point_id')) {
                $status = 0;
                break;
            }
        }

        if ($status == 0) {
            $response = [
                'status' => false,
                'pesan' => 'Kode sudah anda gunakan!'
            ];
        }

        else {    
            $dompet_point = DompetPoint::create($input);
            $data = $dompet_point->toArray();

            $response = [
                'status' => true,
                'pesan' => 'Berhasil',
                'data' => $data
            ];
        }

        return response()->json($response,200);
    }
}
