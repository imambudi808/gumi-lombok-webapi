<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;

class EventController extends Controller
{
    public function index(Request $request){
        $event=Event::offset($request->input('offset'))->limit($request->input('limit'))->orderby('created_at','DESC')->get();
        $data = $event->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data event tampil',
            'data' => $data
        ],200);
    }
}
