<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Order;
use Validator;

class OrderController extends Controller
{
    public function showAll(Request $request){
        $order=Order::where('user_id',$request->input('user_id'))->offset($request->input('offset'))->limit($request->input('limit'))->get();
        $data = $order->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data order berhasil ditampilkan',
            'data' => $data           
            
        ],200);
    }

    public function showOrderByid(Request $request){
        $order=Order::where('order_id',$request->input('order_id'))->get();
        $data = $order->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data order berhasil ditampilkan',
            'data' => $data           
            
        ],200);
    }

    public function insert(Request $request){
        // $orderCode = $this->generateOrderCode();
        $validator = Validator::make($request->all(),[
            // 'order_code' => 'required|orders,order_code'
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'pesan' => 'Validation Error.',
                'data' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        // $dokters->save();
        $code = $this->generateOrderCode();

        // $order = new Order();
        // $order->order_id = $this->generateOrderId();
        // $order->order_code = $this->generateOrderCode();
        // $order->tiket_id = $request->tiket_id;
        $orderId=$this->generateOrderId();
 
       
        $order = Order::create([
            'order_id' => $orderId,
            'order_code' => $this->generateOrderCode(),
            'tiket_id' => $request->input('tiket_id'),
            'tiket_name' => $request->input('tiket_name'),
            'tiket_route' => $request->input('tiket_route'),
            'tiket_agent' => $request->input('tiket_agent'),
            'tiket_price_diskon' => $request->input('tiket_price_diskon'),
            'tiket_keberangkatan' => $request->input('tiket_keberangkatan'),
            'tiket_tiba' => $request->input('tiket'),
            'tiket_detail' => $request->input('tiket_detail'),
            'durasi_tiket' => $request->input('durasi_tiket'),
            'order_tiket_book' => $request->input('order_tiket_book'),
            'order_status' => $request->input('order_status'),
            'created_at' => $request->input('created_at'),
            'updated_at' => $request->input('updated_at'),
            'trash' => $request->input('trash'),
            'metode_bayar' => $request->input('metode_bayar'),
            'bank_tujuan' => $request->input('bank_tujuan'),
            'nama_pemilik_rekening' => $request->input('nama_pemilik_rekening'),
            'tiket_price_adult' => $request->input('tiket_price_adult'),
            'tiket_price_chil' => $request->input('tiket_price_chil'),
            'jumlah_adult' => $request->input('jumlah_adult'),
            'jumlah_anak' => $request->input('jumlah_anak'),
            'total_bayar' => $request->input('total_bayar'),
            'user_id' => $request->input('user_id')
            
        ]);

        $data = $order->toArray();
        

        $response = [
            'status' => true,
            'pesan' => 'sukses konfirmasi.',
            'data' => $validator->errors()
        ];

        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil diinputkan',
            'data' => [$data],
            'order_id' => $orderId,      
            
        ],200);

    }

    private function generateOrderCode()
    {
        $orderCode = str_random(10);

        if($this->checkOrderCodeExistence($orderCode)){
            return $this->generateOrderCode();
        }

        return $orderCode;
        
    }
    private function checkOrderCodeExistence($orderCode)
    {
        return Order::where('order_code', $orderCode)->exists();
    }
// ////////////////////
    private function generateOrderId()
    {
        $orderId = str_random(10);

        if($this->checkOrderIdExistence($orderId)){
            return $this->generateOrderId();
        }

        return $orderId;
        
    }
    private function checkOrderIdExistence($orderId)
    {
        return Order::where('order_id', $orderId)->exists();
    }



    public function updatemetodebayar(Request $request)
    {
        $this->validate($request, [
            'order_code' => 'unique'
        ]);

        $order = Order::where('order_id', $request->input('order_id'));
        // $order->update($request->except('order_id'));
        
        $order->update(['metode_bayar'=>$request->input('metode_bayar')]);
        $order->update(['bank_tujuan'=>$request->input('bank_tujuan')]);
        $order->update(['norek'=>$request->input('norek')]);
        $order->update(['card_holder'=>$request->input('card_holder')]);
        

        $response = [
            'status' => true,
            'pesan' => 'Berhasil update data order',
            'data' => [$request->all()]
        ];

        return response()->json($response, 200);
    }

    public function konfirorder(Request $request){
        $status = Order::where('order_id',$request->input('order_id'));

        $status->update(['order_status'=>$request->input('order_status')]);
        $status->update(['nama_pemilik_rekening'=>$request->input('nama_pemilik_rekening')]);

        $response = [
            'status' => true,
            'pesan' => 'Berhasil update data order',
            'data' => [$request->all()]
        ];

        return response()->json($response, 200);

    }
}
