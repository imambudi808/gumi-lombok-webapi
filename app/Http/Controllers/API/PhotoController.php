<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Photo;
use Validator;
use Carbon\Carbon;

class PhotoController extends Controller
{
    public function desPhoto(){

        
        $photo = Photo::get();
        $data = $photo->toArray();

        
        return response()->json([
            'status' => true,
            'pesan' => 'data photo des more berhasil ditampilkan',
            'data' => $data            
        ], 200);
    }

    public function inserData(Request $request){
        $photo=Photo::create([
            'filename' => $request->input('filename'),
            'description' => $request->input('description')
        ]);

        $data=$photo->toArray();
        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil diinputkan',
            'data' => [$data]             
            
        ],200);
    }
}
