<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Point;
use Validate;

class PointController extends Controller
{
    public function showAll()
    {
        $point_data = Point::all();
        $data = $point_data->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data           
            
        ],200);
    }

    public function show_by_des_id(Request $request){
        $point_data = Point::where('destination_id',$request->input('destination_id'))->get();
        $data = $point_data->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data           
            
        ],200);
    }
}
