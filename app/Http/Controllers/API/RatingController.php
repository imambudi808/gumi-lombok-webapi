<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rating;
use App\Destination;
use Illuminate\Support\Facades\DB;


class RatingController extends Controller
{
    public function showRatingByDes(Request $request){
        // $rating = Rating::where('destination_id',$request->input('destination_id'))->get();
        $rating = DB::table('ratings')
                ->join('users','ratings.user_id','=','users.id')
                ->select('ratings.*','users.name')
                ->where('ratings.destination_id',$request->input('destination_id'))
                ->get();
        $data = $rating->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data tampil',
            'data' => $data
        ],200);
    }

    public function showTotalRating(Request $request){

        $rating = DB::table('ratings')->select(DB::raw('SUM(rating_value) / COUNT(rating_value) as total_rating'))->where('destination_id',$request->input('destination_id'))->get();
        //des
        $destination = Destination::where('destination_id',$request->input('destination_id'));
        $hasil=0;
        foreach ($rating as $data) {
            // $data = (string) $data;
            
            $hasil=$data->total_rating;
        }
        // dd($hasil);
        $destination->update(['rata_rating'=>$hasil]);   

        
        return response()->json([
            'status' => true,
            'pesan' => 'data tampil',
            'data' => $rating
        ],200);

        
    }

    public function showDesRatingAll(){
       
            
        $destination=Rating::groupBy('destination_id')->get();
        // $destination=Destination::first();
        // $data = $destination->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data destinasi berhasil ditampilkan',
            'data' => $destination
        ], 200);

        
    }

    public function insertRating(Request $request){
        $rating = Rating::create([
            'rating_value' => $request->input('rating_value'),
            'rating_coment' => $request->input('rating_coment'),
            'destination_id' => $request->input('destination_id'),
            'user_id' => $request->input('user_id')
        ]);
        // $rating->rating_value = $request->input('rating_value');
        // $rating->rating_coment = $request->input('rating_coment');
        // $rating->destination_id = $request->input('destination_id');

        $data = $rating->toArray();
        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil diinputkan',
            'data' => [$data]             
            
        ],200);
    }

    public function showRatingByIdAndDes(Request $request){
        // $rating = Rating::where('destination_id',$request->input('destination_id'))->get();
        $rating = Rating::where('destination_id',$request->input('destination_id'))->where('user_id',$request->input('user_id'))->get();
        $data = $rating->toArray();
        if($data==NULL){
            return response()->json([
                'status' => false,
                'pesan' => 'data tampil',
                'data' => [
                    'rating_value'=>NULL,
                    'rating_coment'=>NULL
                ]
            ],200);
        }else{
            return response()->json([
                'status' => true,
                'pesan' => 'data tampil',
                'data' => $data
            ],200);
        }

        
    }

    public function cekRatingUser(Request $request){   
            $rating = Rating::where('user_id', $request->input('user_id'))->where('destination_id',$request->input('destination_id'))->get();
            if(isset($rating)){
                $total_rating=0;
                $komen="";
          
                $var = $rating;
                foreach($var as $a){
                    $total_rating += $a->rating_value;
                    $komen = $a->rating_coment;
                }
                                
        
                if($total_rating)
        
                return response()->json([
                    'status' => true,
                    'pesan' => 'data tampil',
                    'rating_value' => $total_rating,
                    'rating_coment' => $komen
                ],200);
                
            }
            return response()->json([
                'status' => false,
                'pesan' => 'belum ada rating dari anda',//pesan disini
                'rating_value' => 0,  
                'rating_coment' => null          
            ], 401);
           
      
    }

    public function upadateRatingByUser(Request $request){
        $rating=Rating::where('destination_id',$request->input('destination_id'));
        $rating->update(['rating_coment'=>$request->input('rating_coment')]);

        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil di ubah',
            'data' => 'data terubah'
            // 'data' => $request->all()
            
        ],200);
       
    }
}
