<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Service;

class ServiceController extends Controller
{
    public function showServiceByIdTiket(Request $request){
        $services = Service::where('tiket_id',$request->input('tiket_id'))->get();
        $data = $services->toArray();

       
        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data
            
        ], 200);
    }
}
