<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Store;
use App\StoreProduk;
use App\StoreReview;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    public function showAllStore(Request $request){
        $stores = Store::offset($request->input('offset'))->limit($request->input('limit'))->get();
        $data=$stores->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data 
        ],200);

    }

    public function showProdukByStoreId(Request $request){
        $produks = StoreProduk::where('store_id',$request->input('store_id'))->offset($request->input('offset'))->limit($request->input('limit'))->get();
        $data = $produks->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data produk by store id berhasil ditampilkan',
            'data' => $data           
            
        ],200);
    }

    public function showNewStore(Request $request){
        $stores = Store::orderby('created_at','DESC')->limit(5)->get();
        $data=$stores->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data 
        ],200);
    }

    public function showNewProduk(Request $request){
        // $produks = StoreProduk::orderby('created_at','DESC')->limit(6)->get();
        $produks = DB::table('store_produks')
                ->join('stores','store_produks.store_id','=','stores.store_id')
                ->select('store_produks.*','stores.store_name','stores.store_number')->orderby('created_at','DESC')->limit(6)->get();
        $data = $produks->toArray();

        return response()->json([
            'status' => true,
            'pesan' => 'data order berhasil ditampilkan',
            'data' => $data           
            
        ],200);
    }

    //review
    public function showReviewByStoreId(Request $request){
        $review = DB::table('store_reviews')
                ->join('users','store_reviews.user_id','=','users.id')
                ->select('store_reviews.*','users.name')
                ->where('store_reviews.store_id',$request->input('store_id'))
                ->get();
        $data = $review->toArray();

        $isComment=0;
        foreach($review as $user){
            if($user->user_id == $request->input('user_id')){
                $isComment=1;
                break;
            }
        }
        return response()->json([
            'status' => true,
            'pesan' => 'data tampil',
            'is_comment' => $isComment,
            'data' => $data
        ],200);
    }

    
}
