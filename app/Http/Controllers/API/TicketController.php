<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Ticket;

class TicketController extends Controller
{
    public function showall(Request $request)
    {
        $tiket=Ticket::offset($request->input('offset'))->limit($request->input('limit'))->get();
        $data = $tiket->toArray();

        
        return response()->json([
            'status' => true,
            'pesan' => 'data tiket berhasil ditampilkan',
            'data' => $data            
            
        ],200);
    }

    public function showByTiketId(Request $request){
        $tikets = Ticket::where('tiket_id',$request->input('tiket_id'))->get();
        $data = $tikets->toArray();

       
        return response()->json([
            'status' => true,
            'pesan' => 'data berhasil ditampilkan',
            'data' => $data
            
        ], 200);
    }
}
