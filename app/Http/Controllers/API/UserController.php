<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\DompetPoint;
use Validator;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\PasswordRequest;

class UserController extends Controller
{
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request, User $model)
    {
        $model->create($request->merge(['password' => Hash::make($request->get('password'))])->all());
        $success['token'] = $user->createToken('Gumi')->accessToken;
        $success['name'] = $user->name;

        $response = [
            'status' => true,
            'pesan' => 'User register successfully.',
            'data' => $success,
            
        ];

        return response()->json($response, 200);
    }

    public function register(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $response = [
                'status' => false,
                'pesan' => $validator->errors(),
                'data' => 'Semua data harus terisi',
               
            ];
            return response()->json($response, 404);
        }        

        $userRegistred = User::all();
        $status = 1;
        foreach($userRegistred as $userNew){
            if ($userNew->email == $request->input('email')) {
                $status = 0;
                break;
            }
        }
        if ($status == 0) {
            $response = [
                'status' => false,
                'pesan' => 'Email Sudah Digunakan'
            ];
        }else {
            $input['password'] = bcrypt($input['password']);
        // $user = User::create($input);
            $userRegistred = User::create($input);
            $data = $userRegistred->toArray();

            $response = [
                'status' => true,
                'pesan' => 'Registrasi Berhasil',
                'data' => $data
            ];
        }
        
        // $input['password'] = bcrypt($input['password']);
        // $user = User::create($input);
        // $success['token'] = $user->createToken('Gumi')->accessToken;
        // $success['name'] = $user->name;



        // $response = [
        //     'status' => true,
        //     'pesan' => 'User register successfully.',
        //     'data' => $success,
            
        // ];

        return response()->json($response, 200);
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            // $response=[
            //     'token' => $user->createToken('MyApp')->accessToken,
            //     // 'user_data' => $user       
                
            // ];  
            $dompet = DompetPoint::where('user_id',$user->id)->get();            

            $var = $dompet;
            $total = 0;
            foreach($var as $a){
                $total += $a->point_value;
            }          

            return response()->json([
                'status' => true,
                'pesan' => 'login suksess bro',
                'data' => [
                    'id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'email_verified' => $user->email_verified_at,
                    'password' => $user->password,
                    'user_role' => $user->user_role,
                    'user_phone' => $user->user_phone,
                    'user_address' => $user->user_address,
                    'user_photo' => $user->user_photo,
                    'total_point' => $total,
                    'passwordnya' => request('password'),
                    'token' => $user->createToken('GumiLombok')->accessToken
                ]
                
            ], 200);
        } else {           
            // return response()->json(['error' => 'Unauthorised'], 401);i
            return response()->json([
                'status' => false,
                'pesan' => 'Login Gagal',                
            ], 401);
        }
    }

    public function editProfile(Request $request){
        $userModel=User::where('id',$request->input('id'));
        // $userModel->$name=$request->input('name');
        // $userModel->$user_phone=$request->input('user_phone');
        // $userModel->$email=$request->input('email');
        $userModel->update(['name'=>$request->input('name')]);
        $userModel->update(['user_phone'=>$request->input('user_phone')]);
        $userModel->update(['email'=>$request->input('email')]);
        // $data=$userModel->toArray();
        return response()->json([
            'status' => true,
            'pesan' => 'data profilr berhasil diperbaharui',
            'data' => $request->all()            
        ], 200);
    }

    public function passwordUpdate(Request $request)
    {
        $userModel=User::where('id',$request->input('id'));
        $userModel->update(['password' => Hash::make($request->input('password_baru'))]);

        return response()->json([
            'status' => true,
            'pesan' => 'Ganti Password Berhasil',
            'data' => $request->all()            
        ], 200);
    }

    public function cekLastPoitn(Request $request){
        $dompet = DompetPoint::where('user_id',$request->input('user_id'))->get();            

            $var = $dompet;
            $total = 0;
            foreach($var as $a){
                $total += $a->point_value;
            }     
            return response()->json([
                'status' => true,
                'pesan' => 'cek point suksess bro',
                'data' => $total
                
            ], 200);
    }

    // public function password(PasswordRequest $request)
    // {
    //     auth()->user()->update(['password' => Hash::make($request->get('password'))]);

    //     return response()->json([
    //                 'status' => true,
    //                 'pesan' => 'Ganti Password Berhasil',
    //                 'data' => $request->all()            
    //             ], 200);
    // }
}
