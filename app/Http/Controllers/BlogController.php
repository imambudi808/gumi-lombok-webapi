<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datablog=Blog::get();
        return view('blog.index',compact('datablog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('blog.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('file');
        $nama_file = time()."_".$file->getClientOriginalName();
        $tujuan_upload = 'data_file/blogs';
		$file->move($tujuan_upload,$nama_file);

        $destination=Blog::create([
            'blog_judul' => $request->blog_judul,   
            'seo_link' => $request->seo_link,
            'blog_creator' => $request->blog_creator,     
            'blog_isi' => $request->blog_isi,
            'blog_image' => $nama_file,
            
            
        ]);

        // dd($destination);
            
        return redirect()->route('blog.index')->withStatus(__('blog sukses dibuat'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($blog_id)
    {
        $datablog=Blog::where('blog_id',$blog_id);
        $datablog->delete();
        return redirect()->route('blog.index')->withStatus(__('Postingan terhapus'));
    }
}
