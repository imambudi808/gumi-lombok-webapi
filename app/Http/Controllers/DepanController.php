<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Destination;
use App\Blog;

class DepanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datades=Destination::paginate(3);
        $datablog=Blog::where('blog_category','!=','acara')->paginate(6);
        $dataacara=Blog::where('blog_category','acara')->paginate(3);
        return view('depan.index',compact('datades','datablog','dataacara'));
    }

    public function detailBlog($blog_id)
    {
        $datablog=Blog::where('blog_id',$blog_id)->get();
        $populer=Blog::paginate(5);
        // dd($datablog);
        return view('depan.blog_detail',compact('datablog','populer'));
    }

    public function detailDestinasi($destination_id)
    {
        $datadestinasi=Destination::where('destination_id',$destination_id)->get();
        $populer=Blog::paginate(5);
        return view('depan.destinasi_detail',compact('datadestinasi','populer'));
    }

    public function category($blog_category){
        $datacategory=Blog::where('blog_category',$blog_category)->get();
        return view('depan.category',compact('depan','datacategory','blog_category'));
    }

    public function maps(){
        return view('depan.maps');
    }

    public function allDestinasi(){
        $datades=Destination::all();
        return view('depan.semua_destinasi',compact('datades'));
    }

    public function populerPost(){
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('depan.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
