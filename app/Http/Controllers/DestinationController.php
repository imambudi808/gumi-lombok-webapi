<?php

namespace App\Http\Controllers;
use App\Destination;

use Illuminate\Http\Request;

class DestinationController extends Controller
{
    public function index(){
        $datades=Destination::get();
        return view('destination.index',compact('datades'));
    }


    public function create()
    {
        return view('destination.create');
    }

    public function store(Request $request){
        $file = $request->file('file');
        $nama_file = time()."_".$file->getClientOriginalName();
        $tujuan_upload = 'data_file';
		$file->move($tujuan_upload,$nama_file);

        $destination=Destination::create([
            'destination_name' => $request->input('destination_name'),        
            'destination_image' => $nama_file,
            'destination_addres' => $request->input('destination_addres'),
            'destination_author' => $request->input('destination_author'),
            'destination_category' => $request->input('destination_category'),
            'destination_lat' => $request->input('destination_lat'),
            'destination_long' => $request->input('destination_long'),
        ]);
            
        return redirect()->route('destination.index')->withStatus(__('Destination sukses dibuat'));
    }

    public function destroy($destination_id){
        $datades= Destination::where('destination_id',$destination_id);
        $datades->delete();
        // return dd($datades);
        return redirect()->route('destination.index')->withStatus(__('Destination sukses dihapus'));
    }

    public function edit($destination_id){
        $datades=Destination::where('destination_id',$destination_id)->get();
        // return $datades;
        return view('destination.edit',compact('datades'));
    }

    public function update(Request $request, $destination_id)
    {
        $datades=Destination::where('destination_id', $destination_id)->first();
        $datades->destination_name = $request->destination_name;
        $datades->destination_addres = $request->destination_addres;
        $datades->destination_category = $request->destination_category;
        $datades->destination_lat = $request->destination_lat;
        $datades->destination_long = $request->destination_long;
        $datades->destination_author = $request->destination_author;

        $datades->save();
        
        return redirect()->route('destination.index');
        // return $destination_id;
    }
}
