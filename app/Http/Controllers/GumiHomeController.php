<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Event;
use App\Blog;
use App\Destination;
use App\Rating;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;


class GumiHomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datades=Destination::paginate(2);    
        $dataevent=Event::paginate(2);
        $datablog1=Blog::offset(0)->limit(2)->orderby('created_at','DESC')->get();
        $datablog2=Blog::offset(2)->limit(2)->orderby('created_at','DESC')->get();
        $datablog3=Blog::offset(4)->limit(2)->orderby('created_at','DESC')->get();
        return view('gumi_home.index',compact('datades','dataevent','datablog1','datablog2','datablog3'));
    }

    public function destinations(){
        $datades=Destination::paginate(6);
        return view('gumi_destination.index',compact('datades'));
    }
    public function blogs(){    
        $datablog=Blog::orderby('created_at','DESC')->paginate(4);    
        return view('gumi_blog.index',compact('datablog'));
    }
    public function events(){
        $dataevent=Event::orderby('created_at','DESC')->paginate(12);        
        return view('gumi_event.index',compact('dataevent'));
    }
    public function destination_detail($seo_link){
        $datades=Destination::where('seo_link',$seo_link)->get();
        foreach($datades as $des)      
        $same_category=Destination::where('destination_category',$des->destination_category)->get();  
        // $des_comment=Rating::where('destination_id',$des->destination_id)->get();    
        $des_comment = DB::table('ratings')
                ->join('users','ratings.user_id','=','users.id')
                ->select('ratings.*','users.name')
                ->where('ratings.destination_id',$des->destination_id)
                ->get();
        return view('gumi_destination.detail',compact('datades','same_category','des_comment'));
        // dd($des->destination_category);
    }
    public function blog_detail($seo_link){
        $datablog=Blog::where('seo_link',$seo_link)->get();    
        $datanewblog=Blog::orderby('created_at','DESC')->paginate(3);
        foreach($datablog as $blog)
        $same_category=Blog::where('blog_category',$blog->blog_category)->paginate(2);   
        return view('gumi_blog.detail',compact('datablog','datanewblog','same_category'));
    }
    public function event_detail($seo_link){
        $dataevent=Event::where('seo_link',$seo_link)->get();   
        $newevent=Event::orderby('created_at','DESC')->paginate(3);     
        return view('gumi_event.detail',compact('dataevent','newevent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
