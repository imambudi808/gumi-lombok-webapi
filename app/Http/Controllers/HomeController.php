<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        // Auth::routes();
        // if(Auth::user()->user_role == 1){
        //     return view('dashboard');
        // } 
        // else {
        //     return view('/');
        // }
        // if(auth()->user()->user_role){
        //     return view('dashboard');
        // }else{
        //     return redirect(route('depan.index'));
        // }
        return view('dashboard');
        // return dd(auth()->user()->user_role);
    }
}
