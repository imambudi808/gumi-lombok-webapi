<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;


class OrderController extends Controller
{
    public function index(){
        $data=Order::get();
        return view('order.index', compact('data'));
    }

    public function edit($id){
        $data=Order::where('order_id',$id)->get();
        return view('order.edit',compact('data'));
    }

    public function update(Request $request,$id){
        $data=Order::where('order_id',$id);
        $data->update(['order_status'=>$request->order_status]);
        
        return redirect()->route('order.index');
    }
}
