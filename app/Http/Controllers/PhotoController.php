<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photo;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $photos = Photo::all();
        $photos = Photo::get();//memberikan pagination pada laravel

        return response()->json($photos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data =$this->validate($request,[
            'photo' => 'required|image|max:1024',
            'description' => 'required'
        ]);
            // upload foto
        $request->file('photo')->store('folder_photos','public');

        // save to db       

        $phot = Photo::create([
            'filename' => $request->file('photo')->hashName(),
            'description' => $data['description']
        ]);

        return response()->json($phot,201);
    }
    

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //menggunakan masukan id untuk menampilkan photo

        $photo = Photo::findOrfail($id);

        return response()->json($photo);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // find fot from db
        $photo = Photo::find($id);

        //validate request data
        $data = $this->validate($request,[
            'description' => 'required'
        ]);

        // $photo = Photo::update($date);
        $photo->description = $data['description'];
        $photo->save();
        return response()->json($photo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
