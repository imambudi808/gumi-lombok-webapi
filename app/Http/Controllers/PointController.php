<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Point;
use App\Destination;
use Illuminate\Support\Arr;



class PointController extends Controller
{    
  
    public function index()
    {
        $point=Point::get();
        return view('point.index',compact('point'));
    }

    public function create()
    {
        $destinations = Destination::all();
        return view('point.create')->with('destinations', $destinations); 
    }

 
    private function generatePointCode()
    {
        
        $pointCode = str_random(5);

        if($this->checkPointCodeExistence($pointCode)){
            return $this->generatePointCode();
        }

        return $pointCode;
        
    }
    private function checkPointCodeExistence($pointCode)
    {
        return Point::where('point_code', $pointCode)->exists();
    }

    public function store(Request $request)
    {
        $point=Point::create([
            'point_name' => $request->input('point_name'),
            'point_code' => $this->generatePointCode(),
            'point_lat' =>$request->input('point_lat'),
            'point_long' => $request->input('point_long'),
            'point_value' => $request->input('point_value'),
            'destination_id' => $request->input('destination_id'),
        ]);

        return redirect()->route('point.index')->withStatus(__('Point Berhasil Dibuat'));
    }


    public function show($id)
    {
        //
    }


    public function edit($point_id)
    {
        $point=Point::where('point_id',$point_id)->get();
        $destinations = Destination::all();
        return view('point.edit',compact('point'),compact('destinations')); 
    }


    public function update(Request $request, $point_id)
    {
        $point=Point::where('point_id',$point_id);
        $point->update(['point_name'=>$request->point_name]);
        $point->update(['point_lat'=>$request->point_lat]);
        $point->update(['point_long'=>$request->point_long]);
        $point->update(['destination_id'=>$request->destination_id]);
       

        return redirect()->route('point.index');
    }


    public function destroy($point_id)
    {
        $point= Point::where('point_id',$point_id);
        $point->delete();
        // return dd($point);
        return redirect()->route('point.index')->withStatus(__('Point sukses dihapus'));
    }


}
