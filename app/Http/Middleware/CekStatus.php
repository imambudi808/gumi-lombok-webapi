<?php

namespace App\Http\Middleware;

use Closure;

class CekStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = \App\User::where('email', $request->email)->first();
        if ($user->user_role == 1) {
            return redirect('home');
        } else{
            return redirect('/');
        }
        // elseif ($user->status == 'mahasiswa') {
        //     return redirect('/');
        // }

        return $next($request);
    }
}
