<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Island extends Model
{
    public $fillable = [
        'island_id','island_name','created_at'
    ];
}
