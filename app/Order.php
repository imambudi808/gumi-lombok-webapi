<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // public $primaryKey = 'order_id';
    public $fillable = [
        'order_id','order_code','tiket_id','tiket_name','tiket_route','tiket_agent','tiket_price_diskon','tiket_keberangkatan','tiket_tiba','tiket_detail','durasi_tiket','order_tiket_book','order_status','created_at','updated_at','trash','metode_bayar','bank_tujuan','norek','card_holder','nama_pemilik_rekening','tiket_price_adult','tiket_price_chil','jumlah_adult','jumlah_anak','total_bayar','user_id'
    ];

    public function Order() {
        return $this->belongsTo('App\User', 'user_id');
    }
}
