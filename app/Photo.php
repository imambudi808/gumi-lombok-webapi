<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    //Add [filename] to fillable property to allow mass assignment on [App\Photo]. jika ada error
    public $fillable = [
        'filename','description','destination_id'
    ];
}
