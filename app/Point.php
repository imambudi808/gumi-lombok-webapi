<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Point extends Model
{
    protected $primaryKey = 'point_id';
    protected $fillable = [
        'point_id','point_code','point_name','point_lat','point_long','point_value','destination_id','created_at','trash'
    ];

    public function Destination() {
        return $this->belongsTo('App\Destination', 'destination_id');
    }

    
}
