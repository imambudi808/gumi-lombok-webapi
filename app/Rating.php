<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    public $fillable = [
        'rating_id','rating_value','rating_coment','destination_id','user_id','created_at','trash'
    ];

    public function RatingToDestination(){
        return $this->belongsTo('App\Destination','destination_id');
    }

    public function RatingToUser(){
        return $this->belongsTo('App\User','user_id');
    }
}
