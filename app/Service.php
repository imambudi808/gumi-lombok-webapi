<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $fillable = [
        'service_id','service_name','tiket_id','created_at','update_at','trash',
    ];
}
