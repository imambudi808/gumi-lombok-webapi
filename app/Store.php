<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    protected $dates = ['created_at'];
    protected $fillable = [
        'store_id','store_name','store_image_profile','store_image_banner','store_de','user_id','created_at','updated_at','seo_link','trash','store_number'
    ];
    protected $primaryKey = 'store_id';
}
