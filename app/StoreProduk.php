<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreProduk extends Model
{
    protected $dates = ['created_at'];
    protected $fillable = [
        'produk_id','produk_name','produk_des','produk_banner','created_at','updated_at','store_id','seo_link','trash','harga_awal'
    ];
    protected $primaryKey = 'produk_id';

    public function ProdukToStore(){
        return $this->belongsTo('App\Store','store_id');
    }
}
