<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StoreReview extends Model
{
    protected $dates = ['created_at'];
    protected $fillable = [
        'review_id','review_rating','review_comment','store_id','user_id','created_at','updated_at'
    ];
    protected $primaryKey = 'review_id';

    public function ReviewToStore(){
        return $this->belongsTo('App\Store','store_id');
    }

    public function ReviewToUser(){
        return $this->belongsTo('App\User','user_id');
    }
}
