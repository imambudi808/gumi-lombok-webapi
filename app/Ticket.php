<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public $fillable = [
        'tiket_id','tiket_name','tiket_route','tiket_agent','tiket_price','tiket_price_diskon','tiket_status','tiket_keberangkatan','tiket_tiba','tiket_detail','tiket_jumlah','created_at','durasi','available_book','end_book','tiket_price_chil'
    ];
}
