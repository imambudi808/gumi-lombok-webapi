<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TravelAgent extends Model
{
    protected $dates = ['created_at'];
    protected $fillable = [
        'agent_id','agent_name','agent_service','agent_phone_number','agent_total_rating','agent_total_comment','agent_image','agent_banner','user_id','created_at','updated_at','seo_link'
    ];
    protected $primaryKey = 'agent_id';
}
