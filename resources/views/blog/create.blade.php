@extends('layouts.app', ['activePage' => 'blog-management', 'titlePage' => __('Blogs Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('blog.store') }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Blog') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('blog.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Blog Judul') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('blog_judul') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('blog_judul') ? ' is-invalid' : '' }}" name="blog_judul" id="input-name" type="text" placeholder="{{ __('Blog Judul') }}" value="{{ old('blog_judul') }}" required="true" aria-required="true"/>
                      @if ($errors->has('blog_judul'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('blog_judul') }}</span>
                      @endif
                    </div>
                  </div>
                </div>   
                
                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Seo Link') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('seo_link') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('seo_link') ? ' is-invalid' : '' }}" name="seo_link" id="input-name" type="text" placeholder="{{ __('Seo Link') }}" value="{{ old('seo_link') }}" required="true" aria-required="true"/>
                      @if ($errors->has('seo_link'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('seo_link') }}</span>
                      @endif
                    </div>
                  </div>
                </div>   

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Blog Creator') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('blog_creator') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('blog_creator') ? ' is-invalid' : '' }}" name="blog_creator" id="input-name" type="text" placeholder="{{ __('Blog Creator') }}" value="{{ old('blog_creator') }}" required="true" aria-required="true"/>
                      @if ($errors->has('blog_creator'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('blog_creator') }}</span>
                      @endif
                    </div>
                  </div>
                </div>   
                
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Blog Isi') }}</label>
                    <div class="col-sm-7">
                      <div class="form-group">
                        <textarea name="blog_isi" id="" cols="30" rows="10"></textarea>
                      </div>
                      {{-- <div class="form-group{{ $errors->has('blog_isi') ? ' has-danger' : '' }}">
                        <input class="form-control{{ $errors->has('blog_isi') ? ' is-invalid' : '' }}" name="blog_isi" id="blog_isi" type="text" placeholder="{{ __('Blog Isi') }}" value="{{ old('blog_isi') }}" required="true" aria-required="true"/>
                        @if ($errors->has('blog_isi'))
                          <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('blog_isi') }}</span>
                        @endif
                      </div> --}}
                    </div>
                  </div>     
                  
             

              


              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Blog Image') }}</label>
                <div class="col-sm-7">                     
                <div>
                  <input type="file" id="input-file-now" class="dropify" data-default-file="" name="file">          
         
                </div>
                </div>
             </div>
              
           
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Add Destination') }}</button>
              </div>
            </div>
          </form>          
        </div>   
      </div>    
        </div>
      </div>
    </div>
  </div>
 
@endsection