@extends('layouts.app', ['activePage' => 'blog-management', 'titlePage' => __('Blogs Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Blog') }}</h4>
                <p class="card-category"> {{ __('Here you can manage blog post') }}</p>
              </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="{{ route('blog.create') }}" class="btn btn-sm btn-primary">{{ __('Add New Post') }}</a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                        <th>
                            {{ __('No') }}
                        </th>
                   
                      <th>
                          {{ __('Judul') }}
                      </th>
                      <th>
                          {{ __('Creator') }}
                      </th>
                      <th>
                        {{ __('Gambar') }}
                      </th>  
                      <th>
                        {{ __('Blog Isi') }}
                      </th> 
                      <th>
                        {{ __('Created At') }}
                      </th>                     
                      <th class="text-right">
                        {{ __('Actions') }}
                      </th>
                    </thead>
                    <tbody>
                        <?php $no = 0;?>
                      @foreach($datablog as $blog)
                      <?php $no++ ;?>
                        <tr>
                          <td>
                            {{ $no }}
                          </td>
                         
                          <td>
                            {{ $blog->blog_judul }}
                          </td>
                          <td>
                            {{ $blog->blog_creator }}
                          </td>
                          <td>
                              <img width="150px" src="{{ url('/data_file/blogs/'.$blog->blog_image) }}">
                          </td>
                          <td>
                            {{ $blog->blog_isi }}
                          </td>
                          <td>
                            {{ $blog->created_at }}
                          </td>
                         
                          
                          <td class="td-actions text-right">
                           
                             <form action="{{ route('blog.destroy',$blog->blog_id) }}" method="post">
                                  @csrf
                                  @method('DELETE')                              
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('blog.edit', $blog->blog_id) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </button>
                              </form>                         
                              
                           
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection