<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Hikers Blog &mdash; Colorlib Website Template</title>
    <meta charset="utf-8">
    @include('depan.header')
  </head>
  <body>
  
  <div class="site-wrap">

    {{-- include navbar --}}
    @include('depan.navbar')
    
    
    <section class="site-section py-lg">
      <div class="container">
        
        <div class="row blog-entries element-animate">
          @foreach ($datablog as $datablog)             
          
          <div class="col-md-12 col-lg-8 main-content">
            
            <div class="post-content-body">
             <h4>{{$datablog->blog_judul}}</h4>
             <span class="d-inline-block mt-1">By <a href="#">Gumi Lombok</a></span>
             <span>&nbsp;-&nbsp; {{$datablog->created_at->format('d-m-Y')}}</span>
            <div class="row mb-5 mt-5">
              <div class="col-md-12 mb-4">
                <img src="{{ url('/data_file/'.$datablog->blog_image) }}" alt="Image placeholder" class="img-fluid rounded">
              </div>              
            </div>
           <div class="row mb-5 mt-5">
             <div class="col-md-12 mb-4">
               {!! $datablog->blog_isi !!}
             </div>
           </div>
            </div>

            
            <div class="pt-5">
              <p>Categories:  <a href="#">{{$datablog->blog_category}}</a></p>
            </div>
            
            


            <div class="pt-5">
                <h6 class="">200 Views</h6>               
                
              {{-- <h3 class="mb-5">6 Comments</h3> --}}
              
              <ul class="comment-list">
                {{-- start coment  --}}
                {{-- <li class="comment">
                  <div class="vcard">
                    <img src="../template_hikers/images/person_1.jpg" alt="Image placeholder">
                  </div>
                  <div class="comment-body">
                    <h3>Jean Doe</h3>
                    <div class="meta">January 9, 2018 at 2:21pm</div>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur quidem laborum necessitatibus, ipsam impedit vitae autem, eum officia, fugiat saepe enim sapiente iste iure! Quam voluptas earum impedit necessitatibus, nihil?</p>
                    <p><a href="#" class="reply rounded">Reply</a></p>
                  </div>
                </li> --}}
                {{-- end coment --}}                
              </ul>
              <!-- END comment-list -->           
              
            </div>
          </div>
          @endforeach
          <!-- END main-content -->
          @include('depan.sidebar')
        </div>
      </div>
    </section>   
    
    @include('depan.footer')
    
  </body>
</html>