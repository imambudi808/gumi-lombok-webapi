<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Gumi Lombok &mdash; Media Infromasi Lombok</title>
    @include('depan.header')
  </head>
  <body>
  
  <div class="site-wrap">

    {{-- include navbar --}}
    @include('depan.navbar')
    
   

    {{-- <div class="site-section"> --}}
      <div class="container">
        <div class="row mb-5">
        <div class="col-12 section-heading"><h2>{{strtoupper($blog_category)}}</h2></div>
        </div>
        <div class="row">
          {{-- mulai --}}
          @foreach ( $datacategory as $datacategory )             
        
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="entry2">
              <a href="{{ route('depan.blog_detail',$datacategory->blog_id) }}"><img src="{{ url('/data_file/'.$datacategory->blog_image) }}" alt="Image" class="img-fluid rounded"></a>
              <span class="post-category text-white bg-success mb-3">{{$datacategory->blog_category}}</span>
              <h2><a href="{{ route('depan.blog_detail',$datacategory->blog_id) }}">{{ $datacategory->blog_judul}}</a></h2>
              <div class="post-meta align-items-center text-left clearfix">
                <figure class="author-figure mb-0 mr-3 float-left"><img src="../templateluar/img/gumiku.png" alt="Image" class="img-fluid"></figure>
                <span class="d-inline-block mt-1">By <a href="#">Gumi Lombok</a></span>
                <span>&nbsp;-&nbsp; {{$datacategory->created_at->format('d-m-Y')}}</span>
              </div>
            {!! $datacategory->blog_isi_singkat !!}
            </div>
          </div>   
          @endforeach
          {{-- selesai --}}
          
        </div>
      </div>
      
    {{-- </div> --}}

   
    @include('depan.footer')
  </body>
</html>