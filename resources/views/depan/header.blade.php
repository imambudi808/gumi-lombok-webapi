<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="shortcut icon" href="../templateluar/img/gumiku.png">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700|Playfair+Display:400,700,900" rel="stylesheet">

    <link rel="stylesheet" href="../template_hikers/fonts/icomoon/style.css">
    <link rel="stylesheet" href="../template_hikers/css/bootstrap.min.css">
    <link rel="stylesheet" href="../template_hikers/css/magnific-popup.css">
    <link rel="stylesheet" href="../template_hikers/css/jquery-ui.css">
    <link rel="stylesheet" href="../template_hikers/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../template_hikers/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../template_hikers/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../template_hikers/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="../template_hikers/css/aos.css">

    <link rel="stylesheet" href="../template_hikers/css/style.css">

    <meta name="dicoding:email" content="imambudi808@gmail.com">
    <!-- Favicon-->
	