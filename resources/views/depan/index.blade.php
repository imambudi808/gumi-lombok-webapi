<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Gumi Lombok &mdash; Media Infromasi Lombok</title>
    @include('depan.header')
  </head>
  <body>
  
  <div class="site-wrap">

    {{-- include navbar --}}
    @include('depan.navbar')
 
    
    <div class="py-5">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="rounded border p-4">
              <div class="row align-items-stretch">
                @foreach ( $datablog as $dataacara )

                <div class="col-md-6 col-lg-4 mb-3 mb-lg-0">
                    <a href="{{ route('depan.blog_detail',$dataacara->blog_id) }}" class="d-flex post-sm-entry">
                      <figure class="mr-3 mb-0"><img src="{{ url('/data_file/'.$dataacara->blog_image) }}" alt="Image" class="rounded"></figure>
                      <div>
                        <span class="post-category bg-danger text-white m-0 mb-2">{{$dataacara->blog_category}}</span>
                        <h2 class="mb-0">{{ $dataacara->blog_judul }}</h2>
                      </div>
                    </a>
                  </div>
                    
                @endforeach              
            
         
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    {{-- <div class="site-section"> --}}
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 section-heading"><h2>Postingan Terbaru</h2></div>
        </div>
        <div class="row">
          {{-- mulai --}}
          @foreach ( $datablog as $datablog )             
        
          <div class="col-lg-4 mb-5 mb-lg-0">
            <div class="entry2">
              <a href="{{ route('depan.blog_detail',$datablog->blog_id) }}"><img src="{{ url('/data_file/'.$datablog->blog_image) }}" alt="Image" class="img-fluid rounded"></a>
              <span class="post-category text-white bg-success mb-3">{{$datablog->blog_category}}</span>
              <h2><a href="{{ route('depan.blog_detail',$datablog->blog_id) }}">{{ $datablog->blog_judul}}</a></h2>
              <div class="post-meta align-items-center text-left clearfix">
                <figure class="author-figure mb-0 mr-3 float-left"><img src="templateluar/img/gumiku.png" alt="Image" class="img-fluid"></figure>
                <span class="d-inline-block mt-1">By <a href="#">Gumi Lombok</a></span>
                <span>&nbsp;-&nbsp; {{$datablog->created_at->format('d-m-Y')}}</span>
              </div>
            {!! $datablog->blog_isi_singkat !!}
            </div>
          </div>   
          @endforeach
          {{-- selesai --}}
          
        </div>
      </div>
      
    {{-- </div> --}}

    <div class="site-section">
      <div class="container">
          <div class="row mb-5">
              <div class="col-12 section-heading"><h2>Wisata Baru</h2></div>
            </div>
        <div class="row align-items-stretch retro-layout">
          {{-- mulai --}}
          @foreach ( $datades as $destinasi )  
          <div class="col-md-4" style="padding-top:10px">
            <a href="{{ route('depan.destinasi_detail',$destinasi->destination_id) }}" class="hentry v-height img-2 gradient" style="background-image: url('{{ url('/data_file/'.$destinasi->destination_image) }}');">
              <span class="post-category text-white bg-primary">{{$destinasi->destination_category}}</span>
              <div class="text text-sm">
                <h2>{{$destinasi->destination_name}}</h2>
                <span>{{$destinasi->created_at->format('d-m-Y')}}</span>
              </div>
            </a>            
          </div>              
          @endforeach

        
        </div>

      </div>
    </div>
    @include('depan.footer')
  </body>
</html>