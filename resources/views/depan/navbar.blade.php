<div class="site-mobile-menu">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
    <div class="site-mobile-menu-body"></div>
  </div>
  
  <header class="site-navbar pt-3" role="banner">
    <div class="container-fluid">
      <div class="row align-items-center">

        <div class="col-6 col-xl-6 logo">
          <h1 class="mb-0"><a href="{{ route('depan.index')}}" class="text-black h2 mb-0"><img src="../templateluar/img/gmweblogo.png" alt="" title="" /></a></h1>
        </div>
        
        <div class="col-6 mr-auto py-3 text-right" style="position: relative; top: 3px;">
          <div class="social-icons d-inline">
            {{-- <a href="#"><span class="icon-facebook"></span></a>
            <a href="#"><span class="icon-twitter"></span></a> --}}
            <a href="https://www.instagram.com/imambudi808/?hl=id"><span class="icon-instagram"></span></a>
          </div>
          <a href="#" class="site-menu-toggle js-menu-toggle text-black d-inline-block d-xl-none"><span class="icon-menu h3"></span></a></div>
        </div>
        
        <div class="col-12 d-none d-xl-block border-top">
          <nav class="site-navigation text-center " role="navigation">

            <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block mb-0">
              <li><a href="{{ route('depan.index')}}">Homepage</a></li>
              {{-- <li class="has-children">
                <a href="category.html">Wistata</a>
                <ul class="dropdown">
                  <li><a href="category.html">Lombok Barat</a></li>
                  <li><a href="category.html">Lombok Utara</a></li>
                  <li><a href="category.html">Lombok Tengah</a></li>
                  <li><a href="category.html">Lombok Timur</a></li>
                  <li><a href="category.html">Mataram</a></li>
                </ul>
              </li> --}}
              <li><a href="{{ route('depan.blog_category','wisata')}}">Wisata</a></li>
              <li><a href="{{ route('depan.blog_category','budaya')}}">Budaya</a></li>             
              <li><a href="{{ route('depan.blog_category','acara') }}">Event</a></li>
              <li><a href="{{ route('depan.semu_destinasi') }}">DestinasiWisata</a></li>
              <li><a href="{{ route('gumi') }}">About</a></li>
              {{-- <li class="has-children">
                <a href="category.html">Wistata</a>
                <ul class="dropdown">
                  <li><a href="category.html">Lombok Barat</a></li>
                  <li><a href="category.html">Lombok Utara</a></li>
                  <li><a href="category.html">Lombok Tengah</a></li>
                  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Lombok Timur</a></li>
                
                </ul>
              </li> --}}
              
              
            </ul>
          </nav>
        </div>
      </div>

    </div>
  </header>   