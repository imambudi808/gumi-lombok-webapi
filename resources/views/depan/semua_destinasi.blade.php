<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Gumi Lombok &mdash; Media Infromasi Lombok</title>
    @include('depan.header')
  </head>
  <body>
  
  <div class="site-wrap">

    {{-- include navbar --}}
    @include('depan.navbar')
    
   

    {{-- <div class="site-section"> --}}
      <div class="container">
        <div class="row mb-5">
        <div class="col-12 section-heading"><h2>DESTINASI WISATA</h2></div>
        </div>
        <div class="row">
          {{-- mulai --}}
          {{-- @foreach ( $datades as $destinasi )   --}}
        
          <div class="row align-items-stretch retro-layout">
                {{-- mulai --}}
                @foreach ( $datades as $destinasi )  
                <div class="col-md-4" style="padding-top:10px">
                  <a href="{{ route('depan.destinasi_detail',$destinasi->destination_id) }}" class="hentry v-height img-2 gradient" style="background-image: url('{{ url('/data_file/'.$destinasi->destination_image) }}');">
                    <span class="post-category text-white bg-primary">{{$destinasi->destination_category}}</span>
                    <div class="text text-sm">
                      <h2>{{$destinasi->destination_name}}</h2>
                      <span>{{$destinasi->created_at->format('d-m-Y')}}</span>
                    </div>
                  </a>            
                </div>              
                @endforeach
      
              
              </div>
          {{-- @endforeach --}}
          {{-- selesai --}}
          
        </div>
      </div>
      <br><br>
      
    {{-- </div> --}}

   
    @include('depan.footer')
  </body>
</html>