
          <div class="col-md-12 col-lg-4 sidebar">
                {{-- <div class="sidebar-box search-form-wrap">
                  <form action="#" class="search-form">
                    <div class="form-group">
                      <span class="icon fa fa-search"></span>
                      <input type="text" class="form-control" id="s" placeholder="Type a keyword and hit enter">
                    </div>
                  </form>
                </div> --}}
                <!-- END sidebar-box -->          
                
                <div class="sidebar-box">
                  <h3 class="heading">Postingan Terbaru</h3>
                  <div class="post-entry-sidebar">
                    <ul>
                      @foreach ($populer as $populer)

                      <li>
                          <a href="{{ route('depan.blog_detail',$populer->blog_id) }}">
                            <img src="{{ url('/data_file/'.$populer->blog_image) }}" alt="Image placeholder" class="mr-4">
                            <div class="text">
                              <h4>{{$populer->blog_judul}}</h4>
                              <div class="post-meta">
                                <span class="mr-2">{{$populer->created_at->format('d-m-Y')}}</span>
                              </div>
                            </div>
                          </a>
                        </li>
                          
                      @endforeach
                      
                     
                     

                    </ul>
                  </div>
                </div>
                <!-- END sidebar-box -->
    
                <div class="sidebar-box">
                  <h3 class="heading">Categories</h3>
                  <ul class="categories">                   

                    <li><a href="{{ route('depan.blog_category','wisata')}}">Wisata<span>(12)</span></a></li>
                    <li><a href="{{ route('depan.blog_category','budaya')}}">Budaya</a></li>             
                    <li><a href="{{ route('depan.blog_category','acara') }}">Event</a></li>
                    <li><a href="{{ route('depan.semu_destinasi') }}">DestinasiWisata</a></li>
                  </ul>
                </div>
                <!-- END sidebar-box -->           
              </div>
              <!-- END sidebar -->
    