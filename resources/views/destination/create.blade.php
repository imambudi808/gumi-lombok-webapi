@extends('layouts.app', ['activePage' => 'destination-management', 'titlePage' => __('Destination Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('destination.store') }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add Destination') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('destination.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('Destination Name') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('destination_name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('destination_name') ? ' is-invalid' : '' }}" name="destination_name" id="input-name" type="text" placeholder="{{ __('Destination Name') }}" value="{{ old('destination_name') }}" required="true" aria-required="true"/>
                      @if ($errors->has('destination_name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('destination_name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>  

                

               
               <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Destination Address') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('destination_addres') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('destination_addres') ? ' is-invalid' : '' }}" name="destination_addres" id="input-name" type="text" placeholder="{{ __('Destination Address') }}" value="{{ old('destination_addres') }}" required="false" aria-required="true"/>
                    @if ($errors->has('destination_addres'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('destination_addres') }}</span>
                    @endif
                  </div>
                </div>                
              </div>   

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Destination Category') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('destination_category') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('destination_category') ? ' is-invalid' : '' }}" name="destination_category" id="input-name" type="text" placeholder="{{ __('Destination Categoty') }}" value="{{ old('destination_category') }}" required="false" aria-required="true"/>
                    @if ($errors->has('destination_category'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('destination_category') }}</span>
                    @endif
                  </div>
                </div>                
              </div>   

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Destination Latitude') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('destination_lat') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('destination_lat') ? ' is-invalid' : '' }}" name="destination_lat" id="input-name" type="text" placeholder="{{ __('Destination Latitude') }}" value="{{ old('destination_lat') }}" required="false" aria-required="true"/>
                    @if ($errors->has('destination_lat'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('destination_lat') }}</span>
                    @endif
                  </div>
                </div>                
              </div>   

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Destination Longitude') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('destination_long') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('destination_long') ? ' is-invalid' : '' }}" name="destination_long" id="input-name" type="text" placeholder="{{ __('Destination Longitude') }}" value="{{ old('destination_long') }}" required="false" aria-required="true"/>
                    @if ($errors->has('destination_long'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('destination_long') }}</span>
                    @endif
                  </div>
                </div>                
              </div>   
              
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Destination Author') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('destination_author') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('destination_author') ? ' is-invalid' : '' }}" name="destination_author" id="input-name" type="text" placeholder="{{ __('Destination Author') }}" value="{{ old('destination_author',auth()->user()->name) }}" required="false" aria-required="true"/>
                    @if ($errors->has('destination_author'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('destination_author') }}</span>
                    @endif
                  </div>
                </div>                
              </div>                


              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('Destination Image') }}</label>
                <div class="col-sm-7">                     
                <div>
                  <input type="file" id="input-file-now" class="dropify" data-default-file="" name="file">          
         
                </div>
                </div>
             </div>              
           
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Add Destination') }}</button>
              </div>
            </div>
          </form>          
        </div>   
      </div>    
        </div>
      </div>
    </div>
  </div>
   
        
@endsection
