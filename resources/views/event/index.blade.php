@extends('layouts.app', ['activePage' => 'event-management', 'titlePage' => __('Event Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Event') }}</h4>
                <p class="card-category"> {{ __('Here you can manage blog post') }}</p>
              </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="{{ route('blog.create') }}" class="btn btn-sm btn-primary">{{ __('Add New Post') }}</a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                        <th>
                            {{ __('No') }}
                        </th>
                   
                      <th>
                          {{ __('Judul event') }}
                      </th>                      
                      <th>
                        {{ __('Gambar') }}
                      </th>  
                      <th>
                        {{ __('Event Cost') }}
                      </th>
                      <th>
                        {{ __('Event Date') }}
                      </th> 
                      <th>
                        {{ __('Created At') }}
                      </th>                     
                      <th class="text-right">
                        {{ __('Actions') }}
                      </th>
                    </thead>
                    <tbody>
                        <?php $no = 0;?>
                      @foreach($dataevent as $event)
                      <?php $no++ ;?>
                        <tr>
                          <td>
                            {{ $no }}
                          </td>
                         
                          <td>
                            {{ $event->event_name }}
                          </td>
                          
                          <td>
                              <img width="150px" src="{{ url('/data_file/event/'.$event->event_image) }}">
                          </td>
                          <td>
                            {{ $event->event_cost }}
                          </td>
                          <td>
                            {{ $event->event_date }}
                          </td>
                          <td>
                            {{ $event->created_at }}
                          </td>
                         
                          
                          {{-- <td class="td-actions text-right">
                           
                             <form action="{{ route('blog.destroy',$blog->blog_id) }}" method="post">
                                  @csrf
                                  @method('DELETE')                              
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('blog.edit', $blog->blog_id) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this user?") }}') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </button>
                              </form>                         
                              
                           
                          </td> --}}
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection