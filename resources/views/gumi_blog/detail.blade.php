@foreach ($datablog as $blog)
    
@endforeach
<!DOCTYPE html>
<html lang="zxx">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="author" content="">
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Gumi-{{$blog->blog_judul}}</title>

@include('gumi_home.header')
<body>

<!-- Wrapper -->
<div id="main_wrapper"> 
  @include('gumi_home.navbar')
  <div class="clearfix"></div>
  
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>{{$blog->blog_judul}}</h2>
          <nav id="breadcrumbs">
            <ul>
              <li><a href="{{ route('gumi_home.index')}}">Home</a></li>
              <li>Blog Detail</li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  
  <div class="container"> 
    <div class="blog-page">
      <div class="row"> 
        <div class="col-lg-8 col-md-8"> 
          <div class="utf_blog_post utf_single_post"> 
            <img class="utf_post_img" src="{{ $blog->blog_image }}" alt=""> 
            <div class="utf_post_content">
              <h3>{{$blog->blog_judul}}</h3>
              <ul class="utf_post_text_meta">
                <li>{{$blog->created_at->format('Y-m-d')}}</li>
                <li>By <a href="#">{{$blog->blog_creator}}</a> </li>
                {{-- <li><a href="#">5 Comments</a></li> --}}
              </ul>
              <p>{!!$blog->blog_isi!!}</p>
              {{-- <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.</p>
              <div class="utf_post_quote"> <span class="icon"></span>
                <blockquote> Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing. </blockquote>
              </div>
              <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing.</p>
              <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic type setting, remaining essentially unchanged.</p> --}}
              <div class="social-contact">
              <a href="{{Share::load('http://gumilomb.id/gumi-blog-detail/'.$blog->seo_link, $blog->blog_judul)->facebook()}}" class="facebook-link"><i class="fa fa-facebook"></i> Facebook</a>
              <a href="{{Share::load('http://gumilomb.id/gumi-blog-detail/'.$blog->seo_link, $blog->blog_judul)->twitter()}}" class="twitter-link"><i class="fa fa-twitter"></i> Twitter</a>
              {{-- <a href="{{Share::load('http://gumilomb.id/gumi-blog-detail/'.$blog->seo_link, $blog->blog_judul)->instagram()}}" class="instagram-link"><i class="fa fa-instagram"></i> Instagram</a> --}}
              <a href="{{Share::load('http://gumilomb.id/gumi-blog-detail/'.$blog->seo_link, $blog->blog_judul)->linkedin()}}" class="linkedin-link"><i class="fa fa-linkedin"></i> Linkedin</a>
              {{-- <a href="#" class="youtube-link"><i class="fa fa-youtube-play"></i> Youtube</a> --}}
              </div>
            </div>
          </div>

          <div id="utf_listing_reviews" class="utf_listing_section">
            <h3 class="utf_listing_headline_part margin-top-75 margin-bottom-20">Writer Profile <span></span></h3>
            <div class="clearfix"></div>		  	
            <div class="comments utf_listing_reviews">
              <ul>         
                <li>
                  <div class="avatar"><img src="{{ url('/data_file/'.$blog->blog_creator_image) }}" alt="" /> </div>
                  <div class="utf_comment_content">
                    <div class="utf_arrow_comment"></div>                  
                                       
                    <div class="utf_by_comment">{{$blog->blog_creator}}<span class="date"><i class="fa fa-clock-o"></i> {{$blog->created_at->format('Y-m-d')}}</span> </div>
                    <p>{{$blog->blog_creator_des}}</p>                  
                  </div>
                </li>		
         
              </ul>
            </div>            
            <div class="clearfix"></div>
          </div>       
       
          <div class="clearfix"></div>
          <h4 class="headline_part margin-top-20">Related Posts</h4>
          <div class="row"> 
  
          @foreach ($same_category as $sameCategory)
          <div class="col-md-6 col-sm-12"> <a href="{{route('gumi_blog.detail',$sameCategory->seo_link)}}" class="blog_compact_part-container">
            <div class="blog_compact_part"> <img src="{{ $blog->blog_image }}" alt="">
              <div class="blog_compact_part_content">
                  <h3>{{$sameCategory->blog_judul}}</h3>
                    <ul class="blog_post_tag_part">
                    <li>{{$sameCategory->blog_category}}</li>
                    </ul>
                    {{-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p> --}}
                  </div>
                </div>
                </a> 
          </div>          
          @endforeach
          
          </div>          
          
          {{-- <section class="comments">
            <h4 class="headline_part margin-top-20 margin-bottom-35">Comments <span class="comments-amount">(5)</span></h4>
            <ul>
              <li>
                <div class="avatar"><img src="images/client-avatar1.jpg" alt="" /></div>
                <div class="utf_comment_content">
                  <div class="utf_arrow_comment"></div>
				  <div class="utf_star_rating_section" data-rating="4.5"></div>
                  <div class="utf_by_comment">John Doe<span class="date"><i class="fa fa-clock-o"></i> Feb 02, 2019 / 12:52 pm</span> <a href="#" class="reply">Reply <i class="fa fa-reply"></i></a> </div>
                  <p>Lorem Ipsum is simply dummy text of the printing and type setting industry. Lorem Ipsum has been the industry's standard dummy text.</p>
                </div>
              </li>
            </ul>
          </section> --}}
          <div class="clearfix"></div>
          
          <br>    
        </div>
        
        <div class="col-lg-4 col-md-4">
          <div class="sidebar right"> 
            @include('../gumi_home/sidebar')
						
            <div class="utf_box_widget margin-top-35">
              <h3><i class="sl sl-icon-book-open"></i> Popular Post</h3>
              <ul class="utf_widget_tabs">                
                @foreach ($datanewblog as $newBlog)
                <li>
                  <div class="utf_widget_content">
                    <div class="utf_widget_thum"> <a href="{{route('gumi_blog.detail',$newBlog->seo_link)}}"><img src="{{ $newBlog->blog_image }}" alt=""></a> </div>
                    <div class="utf_widget_text">
                      <h5><a href="{{route('gumi_blog.detail',$newBlog->seo_link)}}">{{$newBlog->blog_judul}}</a></h5>
                      <span><i class="fa fa-clock-o"></i> {{$newBlog->created_at}}</span>
					          </div>
                    <div class="clearfix"></div>
                  </div>
                </li>     
                @endforeach
                          
                
              </ul>
            </div>            
			
        
          </div>
        </div>
      </div>      
    </div>
  </div>
  
  
  
  <!-- Footer -->
  @include('gumi_home.footer')
</body>
</html>