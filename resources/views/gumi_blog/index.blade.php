<!DOCTYPE html>
<html lang="zxx">
<head>
<meta name="author" content="">
<meta name="description" content="">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Gumi Blogs</title>
@include('../gumi_home.header')
</head>
<body>

<!-- Wrapper -->
<div id="main_wrapper"> 
    @include('../gumi_home.navbar')
  <div class="clearfix"></div>
  
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Know More Information About Lombok</h2>
          <nav id="breadcrumbs">
            <ul>
              <li><a href="{{ route('gumi_home.index')}}">Home</a></li>
              <li>Blogs</li>
            </ul>           
          </nav>
        </div>
      </div>
    </div>
  </div>
  
 

  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 listing_grid_item">
        <div class="listing_filter_block">
          <div class="col-md-2 col-xs-2">
            <div class="utf_layout_nav"> <a href="#" class="grid active"><i class="fa fa-th"></i></a></div>
          </div>
          <div class="col-md-10 col-xs-10">           
            
            <div class="sort-by">
              <div class="utf_sort_by_select_item sort_by_margin">
                <select data-placeholder="Categories:" class="utf_chosen_select_single">
                  <option>All Category</option>
                  {{-- <option>Alam</option>
                  <option>Pantai</option>
                  <option>Air Terjun</option>
                  <option>Kerajinan</option>                  --}}
                  
                </select>
              </div>
            </div>            
          </div>
        </div>
        <div class="row">
          @foreach ($datablog as $blog)
          <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="utf_blog_post"> 
            <a href="{{route('gumi_blog.detail',$blog->seo_link)}}" class="utf_post_img"> <img src="{{ $blog->blog_image }}" alt=""> </a> 
            <div class="utf_post_content">
              <h3><a href="{{route('gumi_blog.detail',$blog->seo_link)}}">{{$blog->blog_judul}}</a></h3>
              <ul class="utf_post_text_meta">
              <li>{{$blog->created_at->format('Y-m-d')}}</li>
              {{-- <li>By <a href="#">Tips</a> Admin</li> --}}
              {{-- <li><a href="#">7 Comments</a></li> --}}
              </ul>
              <p>{!!Str::words($blog->blog_isi,10)!!}.<a href="{{route('gumi_blog.detail',$blog->seo_link)}}"></a></p>
              {{-- <a href="{{route('gumi_blog.blogs')}}" class="read-more">Read More <i class="fa fa-angle-right"></i></a>  --}}
            </div>
            </div>
            </div>               
          @endforeach      
         
        </div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="utf_pagination_container_part margin-top-20 margin-bottom-70">
              <nav class="pagination">
                {{$datablog->links()}}             
                
               
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <!-- Sidebar -->
      <div class="col-lg-4 col-md-4">
        <div class="sidebar">
          @include('../gumi_home/sidebar')    
        </div>
      </div>
    </div>
  </div>

  <!-- Footer -->
 @include('../gumi_home.footer')
</body>
</html>