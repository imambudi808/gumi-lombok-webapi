<!DOCTYPE html>
<html lang="zxx">
<head>
<meta name="author" content="">
<meta name="description" content="">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Gumi Destinations</title>
@include('../gumi_home.header')
</head>
<body>

<!-- Wrapper -->
<div id="main_wrapper">
  @include('../gumi_home.navbar')
  <div class="clearfix"></div>
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Torism Pleaces In Lombok To Visit</h2>
          <!-- Breadcrumbs -->
          <nav id="breadcrumbs">
            <ul>
              <li><a href="{{ route('gumi_home.index')}}">Home</a></li>
              <li>Destinations</li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-8 listing_grid_item">
        <div class="listing_filter_block">
          <div class="col-md-2 col-xs-2">
            <div class="utf_layout_nav"> <a href="#" class="grid active"><i class="fa fa-th"></i></a></div>
          </div>
          <div class="col-md-10 col-xs-10">           
            
            <div class="sort-by">
              <div class="utf_sort_by_select_item sort_by_margin">
                <select data-placeholder="Categories:" class="utf_chosen_select_single">
                  <option>All Category</option>
                  {{-- <option>Alam</option>
                  <option>Pantai</option>
                  <option>Air Terjun</option>
                  <option>Kerajinan</option>                  --}}
                  
                </select>
              </div>
            </div>            
          </div>
        </div>
        <div class="row">

      @foreach ($datades as $destination)
        <div class="col-lg-6 col-md-12"> <a href="{{ route('gumi_destination.detail',$destination->seo_link) }}" class="utf_listing_item-container" data-marker-id="2">
            <div class="utf_listing_item"> <img src="{{ $destination->destination_image }}" alt=""> <span class="tag"> {{$destination->destination_category}}</span>
              <div class="utf_listing_item_content"> 
        
              <h3>{{$destination->destination_name}}</h3>
                <span><i class="sl sl-icon-location"></i> {{$destination->destination_addres}}</span>               
        
              </div>
            </div>
      <div class="utf_star_rating_section" data-rating="{{$destination->rata_rating}}">
        <div class="utf_counter_star_rating">({{$destination->rata_rating}})</div>
        <span class="utf_view_count"> {{$destination->jumlah_response}} reviews</span>
        
      </div>
            </a> 
      </div>          
      @endforeach
          
         
        </div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="utf_pagination_container_part margin-top-20 margin-bottom-70">
              <nav class="pagination">
                {{$datades->links()}}               
                
                {{-- <ul>                  
                  <li><a href="#"><i class="sl sl-icon-arrow-left"></i></a></li>
                  <li><a href="#" class="current-page">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="{{$destination->nextPageUrl}}"><i class="sl sl-icon-arrow-right"></i></a></li>
                </ul> --}}
              </nav>
            </div>
          </div>
        </div>
      </div>
      
      <!-- Sidebar -->
      <div class="col-lg-4 col-md-4">
        <div class="sidebar">
          @include('../gumi_home/sidebar')    
        </div>
      </div>
    </div>
  </div>
  
  
  
  <!-- Footer -->
  @include('../gumi_home.footer')
</body>
</html>