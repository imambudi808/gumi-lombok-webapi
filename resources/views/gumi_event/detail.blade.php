@foreach ($dataevent as $event)
	
@endforeach
<!DOCTYPE html>
<html lang="zxx">
<head>
<meta name="author" content="">
<meta name="description" content="">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Gumi-{{$event->event_name}}</title>

@include('gumi_home.header')
</head>
<body>

<!-- Wrapper -->
<div id="main_wrapper">
  @include('gumi_home.navbar')
  {{-- <div class="clearfix"></div>
  <div id="utf_listing_gallery_part" class="utf_listing_section">
    <div class="utf_listing_slider utf_gallery_container margin-bottom-0"> 
		<a href="{{ url('/data_file/'.$destinations->destination_image) }}" data-background-image="{{ url('/data_file/'.$destinations->destination_image) }}" class="item utf_gallery"></a> 
		<a href="../gumi_template/images/single-listing-02.jpg" data-background-image="../gumi_template/images/single-listing-02.jpg" class="item utf_gallery"></a> 
		<a href="../gumi_template/images/single-listing-03.jpg" data-background-image="../gumi_template/images/single-listing-03.jpg" class="item utf_gallery"></a> 
		<a href="../gumi_template/images/single-listing-04.jpg" data-background-image="../gumi_template/images/single-listing-04.jpg" class="item utf_gallery"></a> 
	</div>
  </div>     --}}

  <div class="container">
    <div class="row utf_sticky_main_wrapper">
      <div class="col-lg-8 col-md-8">
        <div id="titlebar" class="utf_listing_titlebar">
          <div class="utf_listing_titlebar_title">
           <h2>{{$event->event_name}}<span class="listing-tag">{{$event->event_cost}}</span></h2>		   
            <span> <a href="#utf_listing_location" class="listing-address"> <i class="sl sl-icon-location"></i> {{$event->event_location}} </a> </span>			
			{{-- <span class="call_now"><i class="sl sl-icon-phone"></i> (415) 796-3633</span> --}}
            {{-- <div class="utf_star_rating_section" data-rating="{{$event->event_}}">
              <div class="utf_counter_star_rating">({{$destinations->rata_rating}}) / ({{$destinations->jumlah_response}} Reviews)</div>
            </div> --}}
            <ul class="listing_item_social">
              {{-- <li><a href="#"><i class="fa fa-bookmark"></i> Bookmark</a></li>
			  <li><a href="#"><i class="fa fa-star"></i> Add Review</a></li>              
              <li><a href="#"><i class="fa fa-flag"></i> Featured</a></li> --}}
			  <li><a href="#"><i class="fa fa-share"></i> Share</a></li>
			  <li><a href="#"><i class="fa fa-location"></i> Book Now</a></li>
			  {{-- <li><a href="#" class="now_open">Visit Now</a></li> --}}
            </ul>			
          </div>
        </div>
        <div id="utf_listing_overview" class="utf_listing_section">
          <h3 class="utf_listing_headline_part margin-top-30 margin-bottom-30">Event Poster</h3>
          <img class="utf_post_img" src="{{ $event->event_image }}" alt=""> 
          {{-- {{$destinations->destination_content}}disini kontenya ksksksksksk ks ks kk --}}
		  {{-- <div id="utf_listing_tags" class="utf_listing_section listing_tags_section margin-bottom-10 margin-top-0">          
		    <a href="#"><i class="sl sl-icon-phone" aria-hidden="true"></i> +(01) 1123-254-456</a>			
			<a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i> info@example.com</a>	
			<a href="#"><i class="sl sl-icon-globe" aria-hidden="true"></i> www.example.com</a>			
          </div> --}}
		  <div class="social-contact">
			<a href="{{Share::load('http://gumilomb.id/gumi-event-detail/'.$event->seo_link, $event->event_name)->facebook()}}" class="facebook-link"><i class="fa fa-facebook"></i> Facebook</a>
			<a href="{{Share::load('http://gumilomb.id/gumi-event-detail/'.$event->seo_link, $event->event_name)->twitter()}}" class="twitter-link"><i class="fa fa-twitter"></i> Twitter</a>
			{{-- <a href="{{Share::load('http://gumilomb.id/gumi-blog-detail/'.$blog->seo_link, $blog->blog_judul)->instagram()}}" class="instagram-link"><i class="fa fa-instagram"></i> Instagram</a> --}}
			<a href="{{Share::load('http://gumilomb.id/gumi-event-detail/'.$event->seo_link, $event->event_name)->linkedin()}}" class="linkedin-link"><i class="fa fa-linkedin"></i> Linkedin</a>
			{{-- <a href="#" class="youtube-link"><i class="fa fa-youtube-play"></i> Youtube</a> --}}
		  </div>		  		 
        </div>	
        
           
      </div>
      
      <!-- Sidebar -->
      <div class="col-lg-4 col-md-4 margin-top-75 sidebar-search">
        <div class="verified-badge with-tip margin-bottom-30" data-tip-content="Listing has been verified and belongs business owner or manager."> <i class="sl sl-icon-check"></i> Book Now</div>
   
        
       @include('../gumi_home/sidebar')
	
      </div>
    </div>
  </div>
  
  <section class="fullwidth_block padding-top-20 padding-bottom-50">
    <div class="container">
      <div class="row slick_carousel_slider">
        <div class="col-md-12">
			<h3 class="utf_listing_headline_part margin-top-75 margin-bottom-20">More Events </h3>
        </div>		
		<div class="row">
			<div class="col-md-12">
				<div class="simple_slick_carousel_block utf_dots_nav"> 
					@foreach ($newevent as $newEvent)				
				
				  
				  <div class="utf_carousel_item"> <a href="{{ route('gumi_event.detail',$newEvent->seo_link) }}" class="utf_listing_item-container compact">
					<div class="utf_listing_item"> <img src="{{ $newEvent->event_image }}" alt=""> <span class="tag">{{$newEvent->event_cost}}</span>
					  <div class="utf_listing_item_content">
					   
						<h3>{{$newEvent->event_name}}</h3>
						<span><i class="sl sl-icon-location"></i> {{$newEvent->event_location}}</span>																
					  </div>
					</div>
				
					</a> 
				  </div>
				  	
					@endforeach
			
				 
				</div>
			  </div>
		  </div>
	   </div>
    </div>
  </section> 
  
  
  <!-- Footer -->
@include('gumi_home.footer')



<!-- Maps --> 
<script src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script> 
<script src="../gumi_template/scripts/infobox.min.js"></script> 
<script src="../gumi_template/scripts/markerclusterer.js"></script> 
<script src="../gumi_template/scripts/maps.js"></script>
<script src="../gumi_template/scripts/quantityButtons.js"></script>
<script src="../gumi_template/scripts/moment.min.js"></script>
<script src="../gumi_template/scripts/daterangepicker.js"></script>
<script>
$(function() {
	$('#date-picker').daterangepicker({
		"opens": "left",
		singleDatePicker: true,
		isInvalidDate: function(date) {
		var disabled_start = moment('09/02/2018', 'MM/DD/YYYY');
		var disabled_end = moment('09/06/2018', 'MM/DD/YYYY');
		return date.isAfter(disabled_start) && date.isBefore(disabled_end);
		}
	});
});

$('#date-picker').on('showCalendar.daterangepicker', function(ev, picker) {
	$('.daterangepicker').addClass('calendar-animated');
});
$('#date-picker').on('show.daterangepicker', function(ev, picker) {
	$('.daterangepicker').addClass('calendar-visible');
	$('.daterangepicker').removeClass('calendar-hidden');
});
$('#date-picker').on('hide.daterangepicker', function(ev, picker) {
	$('.daterangepicker').removeClass('calendar-visible');
	$('.daterangepicker').addClass('calendar-hidden');
});

function close_panel_dropdown() {
$('.panel-dropdown').removeClass("active");
	$('.fs-inner-container.content').removeClass("faded-out");
}
$('.panel-dropdown a').on('click', function(e) {
	if ($(this).parent().is(".active")) {
		close_panel_dropdown();
	} else {
		close_panel_dropdown();
		$(this).parent().addClass('active');
		$('.fs-inner-container.content').addClass("faded-out");
	}
	e.preventDefault();
});
$('.panel-buttons button').on('click', function(e) {
	$('.panel-dropdown').removeClass('active');
	$('.fs-inner-container.content').removeClass("faded-out");
});
var mouse_is_inside = false;
$('.panel-dropdown').hover(function() {
	mouse_is_inside = true;
}, function() {
	mouse_is_inside = false;
});
$("body").mouseup(function() {
	if (!mouse_is_inside) close_panel_dropdown();
});
</script>
</body>
</html>