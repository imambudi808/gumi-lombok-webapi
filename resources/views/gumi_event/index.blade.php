<!DOCTYPE html>
<html lang="zxx">
<head>
<meta name="author" content="">
<meta name="description" content="">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Gumi Events</title>
@include('../gumi_home.header')
</head>
<body>

<!-- Wrapper -->
<div id="main_wrapper">
  @include('../gumi_home.navbar')
  <div class="clearfix"></div>
  <div id="titlebar" class="gradient">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h2>Events and Festivals in Lombok</h2>
          <!-- Breadcrumbs -->
          <nav id="breadcrumbs">
            <ul>
              <li><a href="{{ route('gumi_home.index')}}">Home</a></li>
              <li>Events</li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 listing_grid_item">        
        <div class="row">
          @foreach ($dataevent as $event)
            <div class="col-lg-4 col-md-12"> <a href="{{route('gumi_event.detail',$event->seo_link)}}" class="utf_listing_item-container" data-marker-id="3">
              <div class="utf_listing_item"> <img src="{{$event->event_image}}" alt=""> 
        
                <div class="utf_listing_item_content"> 
            <div class="utf_listing_prige_block">							
              <span class="utf_meta_listing_price"><i class="fa fa-tag"></i>{{$event->event_cost}}</span>					
              {{-- <span class="utp_approve_item"><i class="utf_approve_listing"></i></span> --}}
            </div>
                    <h3>{{$event->event_name}}</h3>
          <span><i class="sl sl-icon-location"></i> {{$event->event_location}}</span>                                
            <span><i class="sl sl-icon-clock"></i> {{$event->event_date}}</span>
                  </div>
                </div>
          
                </a> 
          </div>              
          @endforeach         
          
          
        </div>
        <div class="clearfix"></div>
        <div class="row">
          <div class="col-md-12">
            <div class="utf_pagination_container_part margin-top-20 margin-bottom-70">
              <nav class="pagination">
                {{$dataevent->links()}} 
                {{-- <ul>
                  <li><a href="#"><i class="sl sl-icon-arrow-left"></i></a></li>
                  <li><a href="#" class="current-page">1</a></li>
                  <li><a href="#">2</a></li>
                  <li><a href="#">3</a></li>
                  <li><a href="#">4</a></li>
                  <li><a href="#"><i class="sl sl-icon-arrow-right"></i></a></li>
                </ul> --}}
              </nav>
            </div>
          </div>
        </div>
      </div>     
    
    </div>
  </div>
  
  
  
  <!-- Footer -->
  @include('../gumi_home.footer')
</body>
</html>