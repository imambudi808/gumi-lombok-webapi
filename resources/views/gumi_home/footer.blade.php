<!-- Footer -->
<div id="footer" class="footer_sticky_part"> 
    <div class="container">
      <div class="row">    
       
       
        <div class="col-md-4 col-sm-12 col-xs-12"> 
          <h4>Connect With Us</h4>
          <p>           
            <a href="https://www.instagram.com/gumilombok_id/?hl=id"><span class="fa fa-instagram fa-2x"></span></a>   
            {{-- <a href="https://www.instagram.com/imambudi808/?hl=id"><span class="fa fa-facebook fa-2x"></span></a>    
            <a href="https://www.instagram.com/imambudi808/?hl=id"><span class="fa fa-twitter fa-2x"></span></a>          --}}
          </p>
          
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4"> </div>
        {{-- <div class="col-md-2 col-sm-4 col-xs-4"> </div> --}}

        <div class="col-md-3 col-sm-4 col-xs-4">         
            
            <a href="https://play.google.com/store/apps/details?id=id.gumilombok"><img src="../gumi_template/images/googleplay.png">  </a>    
          
        </div>
      </div>
      
      <div class="row">
        <div class="col-md-12">
          <div class="footer_copyright_part">Copyright © 2020 Gumi Lombok.</div>
        </div>
      </div>
    </div>
  </div>  
  <div id="bottom_backto_top"><a href="#"></a></div>
</div>

<!-- Scripts --> 
<script src="../gumi_template/scripts/jquery-3.4.1.min.js"></script> 
<script src="../gumi_template/scripts/chosen.min.js"></script> 
<script src="../gumi_template/scripts/slick.min.js"></script> 
<script src="../gumi_template/scripts/rangeslider.min.js"></script> 
<script src="../gumi_template/scripts/magnific-popup.min.js"></script> 
<script src="../gumi_template/scripts/jquery-ui.min.js"></script> 
<script src="../gumi_template/scripts/mmenu.js"></script>
<script src="../gumi_template/scripts/tooltips.min.js"></script> 
<script src="../gumi_template/scripts/color_switcher.js"></script>
<script src="../gumi_template/scripts/jquery_custom.js"></script>


{{-- <!-- Style Switcher -->
<div id="color_switcher_preview">
  <h2>Choose Your Color <a href="#"><i class="fa fa-cog fa-spin (alias)"></i></a></h2>	
	<div>
		<ul class="colors" id="color1">
			<li><a href="#" class="stylesheet"></a></li>
			<li><a href="#" class="stylesheet_1"></a></li>
			<li><a href="#" class="stylesheet_2"></a></li>			
			<li><a href="#" class="stylesheet_3"></a></li>						
			<li><a href="#" class="stylesheet_4"></a></li>
			<li><a href="#" class="stylesheet_5"></a></li>			
		</ul>
	</div>		
</div> --}}
