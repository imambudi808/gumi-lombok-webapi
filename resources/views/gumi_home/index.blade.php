
<!DOCTYPE html>
<html lang="zxx">
<head>
<meta name="author" content="">
<meta name="description" content="">
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Gumi &mdash; Media Infromasi Pariwisata Lombok Terlengkap dan Terpercaya</title>
@include('gumi_home.header')

</head>
<body>

<!-- Wrapper -->
<div id="main_wrapper">
  @include('gumi_home.navbar')
  <div class="clearfix"></div>
  
  <div class="search_container_block home_main_search_part main_search_block" data-background-image="data_file/banner.jpg">
    <div class="main_inner_search_block">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <h2>Find & Explore Nearby Attractions</h2>
            <h4>Find great places in Lombok</h4>
            <div class="main_input_search_part">
              <div class="main_input_search_part_item">
                <input type="text" placeholder="What are you looking for?" value=""/>
              </div>
              
              <button class="button" onclick="window.location.">Search</button>
            </div>
            <div class="main_popular_categories">
			  <h3>Or Browse Popular Categories</h3>	
              <ul class="main_popular_categories_list">		
                <li> <a href="https://play.google.com/store/apps/details?id=id.gumilombok">
                  <div class="utf_box"> <i class="im im-icon-Bus-2" aria-hidden="true"></i>
                    <p>Tour Tickets</p>
                  </div>
                  </a> 
                </li>  
                <li> <a href="{{ route('gumi_destination.destinations')}}">
                  <div class="utf_box"> <i class="im im-icon-Photo" aria-hidden="true"></i>
                    <p>Destinations</p>
                  </div>
                  </a> 
				        </li>    		
                <li> <a href="{{ route('gumi_event.events')}}">
                  <div class="utf_box"> <i class="im im-icon-Electric-Guitar" aria-hidden="true"></i>
                    <p>Events</p>
                  </div>
                  </a> 
                </li>  
                <li> <a href="{{ route('gumi_blog.blogs')}}">
                  <div class="utf_box"> <i class="im im-icon-Newspaper" aria-hidden="true"></i>
                    <p>Blogs</p>
                  </div>
                  </a> 
				        </li>                          
               
               
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>    
  </div>
  
  <div class="container">
	<div class="row">
      <div class="col-md-12">
        <h3 class="headline_part centered margin-top-75"> Most Popular Categories<span>Browse the most desirable categories</span></h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="container_categories_box margin-top-5 margin-bottom-30"> 
          <a href="https://play.google.com/store/apps/details?id=id.gumilombok" class="utf_category_small_box_part"> <i class="im im-icon-Bus-2"></i>
			<h4>Travel Tickets</h4>
			{{-- <span>22</span> --}}
          </a> 
          <a href="{{ route('gumi_destination.destinations')}}" class="utf_category_small_box_part"> <i class="im im-icon-Photo"></i>
			<h4>Destinations</h4>
			{{-- <span>15</span> --}}
          </a> 
          <a href="{{ route('gumi_event.events')}}" class="utf_category_small_box_part"> <i class="im im-icon-Electric-Guitar"></i>
			<h4>Events</h4>
			{{-- <span>05</span> --}}
          </a> 
          <a href="{{ route('gumi_blog.blogs')}}" class="utf_category_small_box_part"> <i class="im im-icon-Newspaper"></i>
			<h4>Blogs</h4>
			{{-- <span>12</span> --}}
          </a> 
      <a href="https://play.google.com/store/apps/details?id=id.gumilombok" class="utf_category_small_box_part"> <i class="im im-icon-Shop"></i>
          <h4>Stores</h4>
      {{-- <span>12</span> --}}
          </a>           
		</div>
		{{-- <div class="col-md-12 centered_content"> <a href="#" class="button border margin-top-20">View More</a> </div> --}}
      </div>
    </div>
  </div>
  
  <section class="fullwidth_block margin-top-65 padding-top-75 padding-bottom-70" data-background-color="#f9f9f9">
    <div class="container">
      <div class="row slick_carousel_slider">
        <div class="col-md-12">
          <h3 class="headline_part centered margin-bottom-45"> Most Visited Places <span>Explore the greates places in the city</span> </h3>
        </div>
		
		<div class="row">
			<div class="col-md-12">
				<div class="simple_slick_carousel_block utf_dots_nav"> 
					@foreach ($datades as $destinations )
					<div class="utf_carousel_item"> <a href="{{ route('gumi_destination.detail',$destinations->seo_link) }}" class="utf_listing_item-container compact">
						<div class="utf_listing_item"> <img src="{{ $destinations->destination_image }}" alt=""> <span class="tag"> Destination</span>
						  <span class="utf_open_now">New</span>
						  <div class="utf_listing_item_content">					    
							<h3>{{$destinations->destination_name}}</h3>
							<span><i class="sl sl-icon-location"></i> {{$destinations->destination_addres}}</span>																
						  </div>					  
						</div>
						<div class="utf_star_rating_section" data-rating="{{$destinations->rata_rating}}">
							<div class="utf_counter_star_rating">({{$destinations->rata_rating}})</div>						
							<span class="utf_view_count"> {{$destinations->jumlah_response}} reviews</span>					
						</div>
						</a> 
					</div>						
					@endforeach

				  
				 @foreach ($dataevent as $events)
				<div class="utf_carousel_item"> <a href="{{route('gumi_event.detail',$events->seo_link)}}" class="utf_listing_item-container compact">
					<div class="utf_listing_item"> <img src="{{ $events->event_image }}" alt=""> <span class="tag"><i class="im im-icon-Electric-Guitar"></i> Events</span>
					  <div class="utf_listing_item_content">
					    <div class="utf_listing_prige_block">							
							<span class="utf_meta_listing_price"><i class="fa fa-tag"></i> {{$events->event_cost}}</span>							
						</div>
						<h3>{{$events->event_name}}</h3>
						<span><i class="sl sl-icon-location"></i> {{$events->event_location}}</span>
						<span><i class="sl sl-icon-clock"></i> {{$events->event_date}}</span>												
					  </div>
					</div>
					
					</a> 
				  </div>
				 @endforeach			  
				  
		
				</div>
			  </div>
		  </div>
	   </div>
    </div>
  </section>  
   
  
  <div class="container padding-bottom-70">
    <div class="row">
      <div class="col-md-12">
        <h3 class="headline_part centered margin-bottom-35 margin-top-70">Blogs <span>Discover best things in Lombok.</span></h3>
	  </div>
	  @foreach ($datablog1 as $blog)
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="utf_blog_post"> 
          <a href="{{route('gumi_blog.detail',$blog->seo_link)}}" class="utf_post_img"> <img src="{{ $blog->blog_image }}" alt=""> </a> 
          <div class="utf_post_content">
            <h3><a href="{{route('gumi_blog.detail',$blog->seo_link)}}">{{$blog->blog_judul}}</a></h3>
            <ul class="utf_post_text_meta">
            <li>{{$blog->created_at->format('Y-m-d')}}</li>
            {{-- <li>By <a href="#">Tips</a> Admin</li>
            <li><a href="#">7 Comments</a></li> --}}
            </ul>
            <p>{!!Str::words($blog->blog_isi,20)!!}.<a href="{{route('gumi_blog.detail',$blog->seo_link)}}"></a></p>
            <a href="{{route('gumi_blog.blogs')}}" class="read-more">Read More <i class="fa fa-angle-right"></i></a> 
          </div>
          </div>
            </div>            
        @endforeach         
	  {{-- @foreach ($datablog1 as $blog1)
	  <div class="col-md-3"> 
		<a href="{{route('gumi_blog.detail',$blog1->seo_link)}}" class="img-box" data-background-image="{{ url('/data_file/'.$blog1->blog_image) }}">
		   <div class="utf_img_content_box visible">
			
			 <span>{{$blog1->blog_category}}</span> 
		   </div>
		</a> 
	  </div>
	  @endforeach
     

	  @foreach ($datablog2 as $blog2)
	  <div class="col-md-6"> 
		<a href="{{route('gumi_blog.detail',$blog2->seo_link)}}" class="img-box" data-background-image="{{ url('/data_file/'.$blog2->blog_image) }}">
		   <div class="utf_img_content_box visible">
			 
			 <span>{{$blog2->blog_category}}</span> 
		   </div>
		</a> 
	 </div>
	  @endforeach
  
	  @foreach ($datablog3 as $blog3)
	  <div class="col-md-3"> 
		<a href="{{route('gumi_blog.detail',$blog3->seo_link)}}" class="img-box" data-background-image="{{ url('/data_file/'.$blog3->blog_image) }}">
		   <div class="utf_img_content_box visible">
			
		   <span>{{$blog3->blog_category}}</span> 
		   </div>
		</a> 
	 </div>
	  @endforeach --}}
      
     
	  <div class="col-md-12 centered_content"> <a href="{{ route('gumi_blog.blogs')}}" class="button border margin-top-20">View More</a> </div>
    </div>
  </div>   
  

  @include('gumi_home.footer')
</body>
</html>