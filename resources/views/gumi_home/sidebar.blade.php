<div class="utf_box_widget">
    <h3><i class="sl sl-icon-magnifier"></i> Search Blog</h3>
    <div class="utf_search_blog_input">
      <div class="input">
        <input class="search-field" type="text" placeholder="Search..." value=""/>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  
<div class="utf_box_widget margin-top-35">
  <h3><i class="sl sl-icon-folder-alt"></i> Categories</h3>
  <ul class="utf_listing_detail_sidebar">
  <li><i class="fa fa-angle-double-right"></i> <a href="#">Travel</a></li>
  <li><i class="fa fa-angle-double-right"></i> <a href="#">Festival</a></li>
  <li><i class="fa fa-angle-double-right"></i> <a href="#">Destination</a></li>
          
  </ul>
</div>