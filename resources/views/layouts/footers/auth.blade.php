<footer class="footer">
  <div class="container">
      <nav class="float-left">
      <ul>
          <li>
          <a href="">
              {{ __('Creative Tim') }}
          </a>
          </li>
          <li>
          <a href="">
              {{ __('About Us') }}
          </a>
          </li>
          <li>
          <a href="">
              {{ __('Blog') }}
          </a>
          </li>
          <li>
          <a href="">
              {{ __('Licenses') }}
          </a>
          </li>
      </ul>
      </nav>
      <div class="copyright float-right">
      &copy;
      <script>
          document.write(new Date().getFullYear())
      </script><i class="material-icons"></i> 
      <a href="gumilombok.id" target="_blank">Material design</a>  <a href="gumilombok.id" target="_blank"></a>
      </div>
  </div>
</footer>