<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="gumilombok.id/" class="simple-text logo-normal">
      {{ __('Gumi Lombok') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      
     
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'profile' || $activePage == 'user-management') ? ' active' : '' }}">
        <a class="nav-link" data-toggle="collapse" href="#laravelExample" aria-expanded="true">
          <i><img style="width:25px" src="{{ asset('material') }}/img/laravel.svg"></i>
          <p>{{ __('Management') }}
            <b class="caret"></b>
          </p>
        </a>
        <div class="collapse show" id="laravelExample">
          <ul class="nav">
            <li class="nav-item{{ $activePage == 'profile' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('profile.edit') }}">
                <span class="sidebar-mini"> UP </span>
                <span class="sidebar-normal">{{ __('User profile') }} </span>
              </a>
            </li>
            <li class="nav-item{{ $activePage == 'user-management' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('user.index') }}">
                <span class="sidebar-mini"> UM </span>
                <span class="sidebar-normal"> {{ __('User Management') }} </span>
              </a>
            </li>
            {{-- destination --}}
            <li class="nav-item{{ $activePage == 'destination-management' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('destination.index') }}">
                <span class="sidebar-mini"> DM </span>
                <span class="sidebar-normal"> {{ __('Destination Management') }} </span>
              </a>
            </li>
            {{-- Point --}}
            <li class="nav-item{{ $activePage == 'point-management' ? ' active' : '' }}">
                <a class="nav-link" href="{{ route('point.index') }}">
                  <span class="sidebar-mini"> PM </span>
                  <span class="sidebar-normal"> {{ __('Point Management') }} </span>
                </a>
            </li>
            {{-- Order --}}
            <li class="nav-item{{ $activePage == 'order-management' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('order.index') }}">
                <span class="sidebar-mini"> OM </span>
                <span class="sidebar-normal"> {{ __('Order Management') }} </span>
              </a>
          </li>
          {{-- Tiket --}}
          <li class="nav-item{{ $activePage == 'tiket-management' ? ' active' : '' }}">
              <a class="nav-link" href="{{ route('tiket.index') }}">
                <span class="sidebar-mini"> TM </span>
                <span class="sidebar-normal"> {{ __('tiket Management') }} </span>
              </a>
          </li>
          {{-- Blog --}}
          <li class="nav-item{{ $activePage == 'blog-management' ? ' active' : '' }}">
            <a class="nav-link" href="{{ route('blog.index') }}">
              <span class="sidebar-mini"> TM </span>
              <span class="sidebar-normal"> {{ __('Blogs Management') }} </span>
            </a>
           </li>
           {{-- Event --}}
          <li class="nav-item{{ $activePage == 'event_management' ? ' active' : '' }}">
            <a class="nav-link" href="{{ route('event.index') }}">
              <span class="sidebar-mini"> TM </span>
              <span class="sidebar-normal"> {{ __('Event Management') }} </span>
            </a>
           </li>
          </ul>
        </div>
      </li>
      {{-- <li class="nav-item{{ $activePage == 'table' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('table') }}">
          <i class="material-icons">content_paste</i>
            <p>{{ __('Table List') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'typography' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('typography') }}">
          <i class="material-icons">library_books</i>
            <p>{{ __('Typography') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'icons' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('icons') }}">
          <i class="material-icons">bubble_chart</i>
          <p>{{ __('Icons') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'map' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('map') }}">
          <i class="material-icons">location_ons</i>
            <p>{{ __('Maps') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'notifications' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('notifications') }}">
          <i class="material-icons">notifications</i>
          <p>{{ __('Notifications') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'language' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('language') }}">
          <i class="material-icons">language</i>
          <p>{{ __('RTL Support') }}</p>
        </a>
      </li>
      <li class="nav-item active-pro{{ $activePage == 'upgrade' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('upgrade') }}">
          <i class="material-icons">unarchive</i>
          <p>{{ __('Upgrade to PRO') }}</p>
        </a>
      </li> --}}
    </ul>
  </div>
</div>