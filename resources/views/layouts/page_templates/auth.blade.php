<div class="wrapper ">
  
      @if(auth()->user()->user_role==1)         
        @include('layouts.navbars.sidebar') 
      @else
        @include('layouts.navbars.sidebar2') 
      @endif
  {{-- @include('layouts.navbars.sidebar') --}}
  <div class="main-panel">
    @include('layouts.navbars.navs.auth')
    @yield('content')
    @include('layouts.footers.auth')
  </div>
</div>