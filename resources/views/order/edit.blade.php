@extends('layouts.app', ['activePage' => 'order-management', 'titlePage' => __('Order Manajemen')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            @foreach($data as $order)
        <form action="{{route('order.update',$order->order_id)}}" method="post">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Edit Dokter') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
              {{-- menampilkan error validasi --}}
                @if ($errors->any())
                  <div class="alert alert-danger alert-with-icon" data-notify="container">
                    <i class="material-icons" data-notify="icon">add_alert</i> Oopss!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                  </div>
                @endif
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('order.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                  </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Status Order') }}</label>
                    <div class="col-sm-7">
                      <select class="custom-select" name="order_status" id="inputGroupSelect03">
                        <option selected>Choose...</option>
                          <option value="Batal">{{ ('Batal') }}</option>                        
                          <option value="Menunggu Pembayaran">{{ ('Menunggu Pembayaran') }}</option>
                          <option value="Menunggu Konfirmasi">{{ ('Menunggu Konfirmasi') }}</option>
                          <option value="Berhasil">{{ ('Berhasil') }}</option>
                          {{-- <option value="Gaga">{{ ('Menunggu Pembayaran') }}</option> --}}
                        
                        </select>   
                    </div>                
                  </div>      
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
              </div>
            </div>
            @endforeach
        </div>
      </div>
    </div>
  </div>
@endsection