@extends('layouts.app', ['activePage' => 'order-management', 'titlePage' => __('Order Manajemen')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-1 text-left">
          <a href="#" class="btn btn-sm btn-primary" style="font-size:15px;" onClick="document.location.reload(true)" ><i class="material-icons">refresh</i></a>
        </div>
        <div class="col-11 text-right">
          {{-- <a href="{{ route('dokter.create') }}" class="btn btn-sm btn-primary" style="font-size:15px;"><i class="material-icons">add_box</i>&nbsp;&nbsp;{{ __('Tambah Dokter') }}</a> --}}
        </div>
       </div>
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header">
                {{-- <form action="/dokter/cari" method="GET">
                  <div class="input-group col-md-12">
                      <input type="text" autocomplete="off" name="keyword" class="form-control" placeholder="Cari Nama Dokter dan tekan Enter">
                      <i class="material-icons">search</i>
                  </div>
                </form> --}}
              </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="table-responsive table-bordered">
                  <table class="table table-bordered table-hover table-striped">
                    <thead class=" text-primary">
                      <th width="1%" class="text-center" style="font-weight:bolder">
                        {{ __('No')}}
                      </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                          {{ __('Nama User') }}
                      </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                        {{ __('Order Id') }}
                    </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                        {{ __('Nama Tiket') }}
                      </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                        {{ __('Tour Agent') }}
                      </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                        {{ __('Tanggal Beli') }}
                      </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                        {{ __('Tanggal Berangkat') }}
                      </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                        {{ __('OrderCode') }}
                      </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                        {{ __('Status Pesanan') }}
                      </th>
                      <th width="10%" class="text-center" style="font-weight:bolder">
                          {{ __('Total Bayar') }}
                        </th>
                      <th width="5%" class="text-center" style="font-weight:bolder">
                        {{ __('Action') }}
                    </thead>
                    <tbody>
                   
                      @foreach($data as $order)
                        <tr>
                        <td>{{$loop->iteration}}</td>
                          <td>
                            {{ $order->Order->name }}
                          </td>
                          <td>
                            {{ $order->order_id }}
                          </td>
                          <td>
                            {{ $order->tiket_name }}
                          </td>
                          <td>
                            {{ $order->tiket_agent }}
                          </td> 
                          <td>
                            {{ $order->created_at }}
                          </td> 
                          <td>
                            {{ "Tanggal Berangkat(Proses)" }}
                          </td> 
                          <td>
                            {{ $order->order_code }}
                          </td> 
                          <td>
                            {{ $order->order_status }}
                          </td> 
                          <td>
                              {{ $order->total_bayar }}
                            </td>
                            <td class="td-actions text-right">
                            
                              <form action="{{ route('order.destroy',$order->order_id) }}" method="post">
                                  @csrf
                                  @method('DELETE')                              
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('order.edit', $order->order_id) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this order?") }}') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </button>
                              </form>                         
                              
                            
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table> 
                </div>
              </div>
           
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection