@extends('layouts.app', ['activePage' => 'point-management', 'titlePage' => __('point Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <form method="post" action="{{ route('point.store') }}" autocomplete="off" class="form-horizontal" enctype="multipart/form-data">
            @csrf
            @method('post')

            <div class="card ">
              <div class="card-header card-header-primary">
                <h4 class="card-title">{{ __('Add point') }}</h4>
                <p class="card-category"></p>
              </div>
              <div class="card-body ">
                <div class="row">
                  <div class="col-md-12 text-right">
                      <a href="{{ route('point.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                  </div>
                </div>

                <div class="row">
                  <label class="col-sm-2 col-form-label">{{ __('point Name') }}</label>
                  <div class="col-sm-7">
                    <div class="form-group{{ $errors->has('point_name') ? ' has-danger' : '' }}">
                      <input class="form-control{{ $errors->has('point_name') ? ' is-invalid' : '' }}" name="point_name" id="input-name" type="text" placeholder="{{ __('point Name') }}" value="{{ old('point_name') }}" required="true" aria-required="true"/>
                      @if ($errors->has('point_name'))
                        <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('point_name') }}</span>
                      @endif
                    </div>
                  </div>
                </div>               
              

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('point Latitude') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('point_lat') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('point_lat') ? ' is-invalid' : '' }}" name="point_lat" id="input-name" type="text" placeholder="{{ __('point Latitude') }}" value="{{ old('point_lat') }}" required="false" aria-required="true"/>
                    @if ($errors->has('point_lat'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('point_lat') }}</span>
                    @endif
                  </div>
                </div>                
              </div>   

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('point Longitude') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('point_long') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('point_long') ? ' is-invalid' : '' }}" name="point_long" id="input-name" type="text" placeholder="{{ __('point Longitude') }}" value="{{ old('point_long') }}" required="false" aria-required="true"/>
                    @if ($errors->has('point_long'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('point_long') }}</span>
                    @endif
                  </div>
                </div>                
              </div>   
              
              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('point value') }}</label>
                <div class="col-sm-7">
                  <div class="form-group{{ $errors->has('point_value') ? ' has-danger' : '' }}">
                    <input class="form-control{{ $errors->has('point_author') ? ' is-invalid' : '' }}" name="point_value" id="input-name" type="text" placeholder="{{ __('point value') }}" value="{{ old('point_value') }}" required="false" aria-required="true"/>
                    @if ($errors->has('point_value'))
                      <span id="name-error" class="error text-danger" for="input-name">{{ $errors->first('point_value') }}</span>
                    @endif
                  </div>
                </div>                
              </div>   

              <div class="row">
                <label class="col-sm-2 col-form-label">{{ __('destinasi') }}</label>
                <div class="col-sm-7">
                  <select class="custom-select" name="destination_id" id="inputGroupSelect03">
                    <option selected>Choose...</option>
                    @foreach($destinations as $destination)
                      <option value="{{ $destination->destination_id }}">{{ $destination->destination_name }}</option>
                    @endforeach
                    </select>   
                </div>                
              </div>                

              
           
              </div>
              <div class="card-footer ml-auto mr-auto">
                <button type="submit" class="btn btn-primary">{{ __('Add point') }}</button>
              </div>
            </div>
          </form>          
        </div>   
      </div>    
        </div>
      </div>
    </div>
  </div>
@endsection