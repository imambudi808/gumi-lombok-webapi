@extends('layouts.app', ['activePage' => 'point-management', 'titlePage' => __('Point Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Point') }}</h4>
                <p class="card-category"> {{ __('Here you can manage point') }}</p>
              </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="{{ route('point.create') }}" class="btn btn-sm btn-primary">{{ __('Add Point Destination') }}</a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                        <th>
                            {{ __('No') }}
                        </th>
                   
                      <th>
                          {{ __('Nama Point') }}
                      </th>
                      <th>
                          {{ __('Point Code') }}
                      </th>
                      <th>
                        {{ __('Point Lokasi') }}
                      </th>  
                      <th>
                        {{ __('Point Value') }}
                      </th> 
                      <th>
                        {{ __('Destinasi') }}
                      </th>                     
                    
                    </thead>
                    <tbody>
                        <?php $no = 0;?>
                      @foreach($point as $point)
                      <?php $no++ ;?>
                        <tr>
                          <td>
                            {{ $no }}
                          </td>
                         
                          <td>
                            {{ $point->point_name }}
                          </td>
                          
                          <td>
                            {{ $point->point_code }}
                          </td>
                          <td>
                            {{ $point->point_lat }} , {{ $point->point_long }}
                          </td>
                          <td>
                            {{ $point->point_value }}
                          </td>
                          <td>
                                {{ $point->Destination->destination_name }}
                        </td>
                          
                          <td class="td-actions text-right">
                           
                             <form action="{{ route('point.destroy',$point->point_id) }}" method="post">
                                  @csrf
                                  @method('DELETE')                              
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('point.edit', $point->point_id) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this point?") }}') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </button>
                              </form>                         
                              
                           
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection