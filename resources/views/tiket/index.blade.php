@extends('layouts.app', ['activePage' => 'tiket-management', 'titlePage' => __('Tiket Management')])

@section('content')
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary">
                <h4 class="card-title ">{{ __('Tiket') }}</h4>
                <p class="card-category"> {{ __('Here you can manage tiket') }}</p>
              </div>
              <div class="card-body">
                @if (session('status'))
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                      </div>
                    </div>
                  </div>
                @endif
                <div class="row">
                  <div class="col-12 text-right">
                    <a href="{{ route('tiket.create') }}" class="btn btn-sm btn-primary">{{ __('Add Tiket Tour') }}</a>
                  </div>
                </div>
                <div class="table-responsive">
                  <table class="table">
                    <thead class=" text-primary">
                        <th>
                            {{ __('No') }}
                        </th>
                   
                      <th>
                          {{ __('Nama Tiket') }}
                      </th>
                      <th>
                          {{ __('Tiket Agent') }}
                      </th>
                      <th>
                        {{ __('Tiket Rute') }}
                      </th>  
                      <th>
                        {{ __('Harga Tiket') }}
                      </th> 
                      <th>
                        {{ __('Action') }}
                      </th>                     
                    
                    </thead>
                    <tbody>
                        <?php $no = 0;?>
                      @foreach($tikets as $tiket)
                      <?php $no++ ;?>
                        <tr>
                          <td>
                            {{ $no }}
                          </td>
                         
                          <td>
                            {{ $tiket->tiket_name }}
                          </td>
                          
                          <td>
                            {{ $tiket->tiket_agent }}
                          </td>
                          <td>
                            {{ $tiket->tiket_route }}
                          </td>
                          <td>
                            {{ $tiket->tiket_price }}
                          </td>
                          
                          
                          <td class="td-actions text-right">
                           
                             <form action="{{ route('tiket.destroy',$tiket->tiket_id) }}" method="post">
                                  @csrf
                                  @method('DELETE')                              
                                  <a rel="tooltip" class="btn btn-success btn-link" href="{{ route('tiket.edit', $tiket->tiket_id) }}" data-original-title="" title="">
                                    <i class="material-icons">edit</i>
                                    <div class="ripple-container"></div>
                                  </a>
                                  <button type="button" class="btn btn-danger btn-link" data-original-title="" title="" onclick="confirm('{{ __("Are you sure you want to delete this tiket?") }}') ? this.parentElement.submit() : ''">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </button>
                              </form>                         
                              
                           
                          </td>
                        </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection