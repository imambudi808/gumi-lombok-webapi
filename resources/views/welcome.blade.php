<!DOCTYPE html>
<html lang="zxx" class="no-js">

<head>
	<!-- Mobile Specific Meta -->
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Favicon-->
	<link rel="shortcut icon" href="templateluar/img/gumiku.png">
	<!-- Author Meta -->
	<meta name="author" content="codepixer">
	<!-- Meta Description -->
	<meta name="description" content="">
	<!-- Meta Keyword -->
	<meta name="keywords" content="">
	<!-- meta character set -->
	<meta charset="UTF-8">
	<!-- Site Title -->
	<title>Gumi Lombok</title>

	<link href="https://fonts.googleapis.com/css?family=Poppins:400,600|Roboto:400,400i,500" rel="stylesheet">
	<!--
			CSS
			============================================= -->
	<link rel="stylesheet" href="templateluar/css/linearicons.css">
	<link rel="stylesheet" href="templateluar/css/font-awesome.min.css">
	<link rel="stylesheet" href="templateluar/css/bootstrap.css">
	<link rel="stylesheet" href="templateluar/css/magnific-popup.css">
	<link rel="stylesheet" href="templateluar/css/nice-select.css">
	<link rel="stylesheet" href="templateluar/css/hexagons.min.css">
	<link rel="stylesheet" href="templateluar/css/owl.carousel.css">
	<link rel="stylesheet" href="templateluar/css/main.css">
</head>

<body>
	<!-- start header Area -->
	<header id="header">
		<div class="container main-menu">
			<div class="row align-items-center justify-content-between d-flex">
				<div id="logo">
					<a href="{{ route('depan.index')}}"><img src="templateluar/img/gmweblogo.png" alt="" title="" /></a>
				</div>
				<nav id="nav-menu-container">
					<ul class="nav-menu">
						{{-- <li class="menu-active"><a href="index.html">Home</a></li> --}}
					<li><a href="{{route('login')}}">Login</a></li>
						{{-- <li><a href="pricing.html">Pricing</a></li>
						<li class="menu-has-children"><a href="">Pages</a>
							<ul>
								<li><a href="elements.html">Elements</a></li>
							</ul>
						</li>
						<li class="menu-has-children"><a href="">Blog</a>
							<ul>
								<li><a href="blog-home.html">Blog Home</a></li>
								<li><a href="blog-single.html">Blog Single</a></li>
							</ul>
						</li>
						<li><a href="contact.html">Contact</a></li> --}}
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- end header Area -->

	<!-- start banner Area -->
	<section class="home-banner-area">
		<div class="container">
			<div class="row fullscreen d-flex align-items-center justify-content-between">
				<div class="home-banner-content col-lg-6 col-md-6">
					<h1>
						Gumi Lombok <br> Teman Traveling Anda
					</h1>
					<p>Aplikasi pemandu wisata daerah Lombok.</p>
					<div class="download-button d-flex flex-row justify-content-start">
						<div class="buttons flex-row d-flex">
							<i class="fa fa-apple" aria-hidden="true"></i>
							<div class="desc">
								<a href="#">
									<p>
										<span>Available</span> <br>
										on App Store
									</p>
								</a>
							</div>
						</div>
						
					</div>
				</div>
				<div class="banner-img col-lg-4 col-md-6">
					<img class="img-fluid" src="templateluar/img/gumibanner.png" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->

	<!-- Start fact Area -->
	<section class="fact-area">
		<div class="container">
			<div class="fact-box">
				<div class="row align-items-center">
					<!-- <div class="col single-fact">
						<h2>100K+</h2>
						<p>Total Downloads</p>
					</div>
					<div class="col single-fact">
						<h2>10K+</h2>
						<p>Positive Reviews</p>
					</div>
					<div class="col single-fact">
						<h2>50K+</h2>
						<p>Daily Visitors</p>
					</div>
					<div class="col single-fact">
						<h2>0.02%</h2>
						<p>Uninstallation Rate</p>
					</div>
					<div class="col single-fact">
						<h2>15K+</h2>
						<p>Pro User</p>
					</div> -->
				</div>
			</div>
		</div>
	</section>
	<!-- End fact Area -->

	<!-- Start feature Area -->
	<section class="feature-area section-gap-top">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="section-title text-center">
						<h2>Fitur Aplikasi</h2>
						<p>Beberapa fitur yang dapat memudahkan anda dalam menjelajahi wisata Lombok.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-feature">
						<a href="#" class="title">
							<span class="lnr lnr-book"></span>
							<h3>Memesan Tiket Tour</h3>
						</a>
						<p>
							Tanda lelah mencari anda dapat melakukan booking tiket tour dengan Agent Tour yang Terpercaya.
						</p>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-feature">
						<a href="#" class="title">
							<span class="lnr lnr-book"></span>
							<h3>Mencari Destinasi</h3>
						</a>
						<p>
							Membantu anda dalam menemukan lokasi wisata yang diinginkan.
						</p>
					</div>
				</div>
				<!-- <div class="col-lg-4 col-md-6">
					<div class="single-feature">
						<a href="#" class="title">
							<span class="lnr lnr-book"></span>
							<h3>Great Support</h3>
						</a>
						<p>
							Usage of the Internet is becoming more common due to rapid advancement of technology and power.
						</p>
					</div>
				</div> -->

			</div>
		</div>
	</section>
	<!-- End feature Area -->

	<!-- Start about Area -->
	<section class="about-area">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-lg-5 home-about-left">
					<img class="img-fluid" src="templateluar/img/detailwisata.png" alt="">
				</div>
				<div class="offset-lg-1 col-lg-5 home-about-right">
					<h1>
						Untuk pencari <br>
						destinasi wisata
					</h1>
					<!-- <p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
						magna aliqua.Ut
						enim ad minim. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
					</p> -->
					<a class="primary-btn text-uppercase" href="#">Get Details</a>
				</div>
				<div class="col-lg-6 home-about-right home-about-right2">
					<h1>
						Tanpa lelah mencari <br>
						temukan Agent Tour Terpercaya
					</h1>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
						magna aliqua.Ut
						enim ad minim. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.
					</p>
					<div class="download-button d-flex flex-row justify-content-start">
						
						<div class="buttons dark flex-row d-flex">
							<i class="fa fa-android" aria-hidden="true"></i>
							<div class="desc">
								<a href="#">
									<p>
										<span>Available</span> <br>
										on Play Store
									</p>
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-5 home-about-left">
					<img class="img-fluid" src="templateluar/img/notatiket.png" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- End about Area -->

	<!-- Start about-video Area -->
	<!-- <section class="about-video-area section-gap">
		<div class="vdo-bg">
			<div class="container">
				<div class="row align-items-center justify-content-center">
					<div class="col-lg-12 about-video-right justify-content-center align-items-center d-flex relative">
						<div class="overlay overlay-bg"></div>
						<a class="play-btn" href="https://www.youtube.com/watch?v=ARA0AxrnHdM"><img class="img-fluid mx-auto" src="templateluar/img/play-btn.png"
							 alt=""></a>
					</div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- End about-video Area -->

	<!-- Start Testimonials Area -->
	<!-- <section class="testimonials-area section-gap-top">
		<div class="container">
			<div class="testi-slider owl-carousel" data-slider-id="1">
				<div class="item">
					<div class="testi-item">
						<img src="templateluar/img/quote.png" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it,
								you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="templateluar/img/quote.png" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it,
								you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="templateluar/img/quote.png" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it,
								you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
				<div class="item">
					<div class="testi-item">
						<img src="templateluar/img/quote.png" alt="">
						<h4>Fanny Spencer</h4>
						<ul class="list">
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
							<li><a href="#"><i class="fa fa-star"></i></a></li>
						</ul>
						<div class="wow fadeIn" data-wow-duration="1s">
							<p>
								As conscious traveling Paup ers we must always be oncerned about our dear Mother Earth. If you think about it,
								you travel
								across her face <br> and She is the host to your journey.
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="owl-thumbs d-flex justify-content-center" data-slider-id="1">
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="templateluar/img/testimonial/t1.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="templateluar/img/testimonial/t2.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="templateluar/img/testimonial/t3.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
				<div class="owl-thumb-item">
					<div>
						<img class="img-fluid" src="templateluar/img/testimonial/t4.jpg" alt="">
					</div>
					<div class="overlay overlay-grad"></div>
				</div>
			</div>
		</div>
	</section> -->
	<!-- End Testimonials Area -->

	<!-- Start Screenshot Area -->
	<section class="section-gap-top">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="section-title text-center">
						<h2>Featured Screens</h2>
						<p>Tampilan aplikasi yang akan anda.
						</p>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="owl-carousel owl-screenshot">
					<div class="single-screenshot">
						<img class="img-fluid" src="templateluar/img/screenshots/1.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="templateluar/img/screenshots/2.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="templateluar/img/screenshots/3.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="templateluar/img/screenshots/4.jpg" alt="">
					</div>
					
				</div>
			</div>
			<br>
			<br>
			<br>
			<div class="row">
				<div class="owl-carousel owl-screenshot">
					<div class="single-screenshot">
						<img class="img-fluid" src="templateluar/img/screenshots/5.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="templateluar/img/screenshots/6.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="templateluar/img/screenshots/7.jpg" alt="">
					</div>
					<div class="single-screenshot">
						<img class="img-fluid" src="templateluar/img/screenshots/8.jpg" alt="">
					</div>
					
				</div>
			</div>
			
		</div>
		
	</section>
	<!-- End Screenshot Area -->

	<!-- Start Pricing Area -->
	<section class="pricing-area">
		<div class="container">
			<div class="row d-flex justify-content-center">
				<div class="col-lg-6">
					<div class="section-title text-center">
						<!-- <h2>Suitable Pricing Plans</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore
							magna aliqua.
						</p> -->
					</div>
				</div>
			</div>

			
		</div>
	</section>
	<!-- End Pricing Area -->

	<!-- Start Footer Area -->
	<footer class="footer-area section-gap">
		<div class="container">
			
			<div class="footer-bottom row align-items-center">
				<p class="footer-text m-0 col-lg-6 col-md-12">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>

				<div class="col-lg-6 col-md-6 social-link">
					<div class="download-button d-flex flex-row justify-content-end">
						
						<div class="buttons gray flex-row d-flex">
							<i class="fa fa-android" aria-hidden="true"></i>
							<div class="desc">
								<a href="#">
									<p>
										<span>Available</span> <br>
										on Play Store
									</p>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<!-- End Footer Area -->

	<script src="templateluar/js/vendor/jquery-2.2.4.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.templateluar/js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
	 crossorigin="anonymous"></script>
	<script src="templateluar/js/tilt.jquery.min.js"></script>
	<script src="templateluar/js/vendor/bootstrap.min.js"></script>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
	<script src="templateluar/js/easing.min.js"></script>
	<script src="templateluar/js/hoverIntent.js"></script>
	<script src="templateluar/js/superfish.min.js"></script>
	<script src="templateluar/js/jquery.ajaxchimp.min.js"></script>
	<script src="templateluar/js/jquery.magnific-popup.min.js"></script>
	<script src="templateluar/js/owl.carousel.min.js"></script>
	<script src="templateluar/js/owl-carousel-thumb.min.js"></script>
	<script src="templateluar/js/hexagons.min.js"></script>
	<script src="templateluar/js/jquery.nice-select.min.js"></script>
	<script src="templateluar/js/waypoints.min.js"></script>
	<script src="templateluar/js/mail-script.js"></script>
	<script src="templateluar/js/main.js"></script>
</body>

</html>