<?php

use Illuminate\Http\Request;

use App\Http\Controllers;;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::post('login', 'Auth\LoginController@login');

// Route::post('login2','LoginControllerApi@login');
// Route::get('myhome','LoginControllerApi@myhome')->middleware('auth:api');

// Route::resource('/photo', 'PhotoController')->middleware('auth:api');

Route::post('register', 'API\UserController@register');
Route::post('store', 'API\UserController@store');
Route::post('login', 'API\UserController@login');
Route::post('update_profile','API\UserController@editProfile');
Route::post('update_password','API\UserController@passwordUpdate');
Route::post('cek_point','API\UserController@cekLastPoitn');

// Route::middleware('auth:api')->group( function () {
//     Route::resource('destination', 'API\DestinationController');
// });
Route::post('destination','API\DestinationController@index');
Route::post('newdestination','API\DestinationController@newDes');

// Route::post('destination','API\DestinationController@index');

//dompet_point_route
Route::post('show_all_dompet_point','API\DompetPointController@show')->middleware('auth:api');
Route::post('show_dompet_point_by_user_id','API\DompetPointController@showByUserId')->middleware('auth:api');
Route::post('insert_point_to_dompet','API\DompetPointController@insertPointToDompet')->middleware('auth:api');

//point route
Route::post('show_all_point','API\PointController@showAll')->middleware('auth:api');;
Route::post('show_point_by_des_id','API\PointController@show_by_des_id')->middleware('auth:api');;

// photo
Route::post('destination_photo','API\PhotoController@desPhoto');
Route::post('insert_photo','API\PhotoController@inserData');

//tiket
Route::post('tiket_toure','API\TicketController@showall');
Route::post('tiket_toure_detail','API\TicketController@showByTiketId');
Route::post('services','API\ServiceController@showServiceByIdTiket');

//order
Route::post('show_orders', 'API\OrderController@showAll')->middleware('auth:api');;
Route::post('insert_orders', 'API\OrderController@insert')->middleware('auth:api');;
Route::post('update_orders', 'API\OrderController@updatemetodebayar')->middleware('auth:api');;
Route::post('update_konfir','API\OrderController@konfirorder')->middleware('auth:api');;
Route::post('order_by_id','API\OrderController@showOrderByid')->middleware('auth:api');;
// Route::post('generateOrderCode','API\OrderController@generateOrderCode');


//Route rating
Route::post('show_rating_by_des','API\RatingController@showRatingByDes');
Route::post('show_total_rate','API\RatingController@showTotalRating');;
Route::post('show_des_rating_all','API\RatingController@showDesRatingAll');;
Route::post('insert_rating','API\RatingController@insertRating');;
Route::post('show_rating_by_des_user','API\RatingController@showRatingByIdAndDes');;
Route::post('cek_rating','API\RatingController@cekRatingUser');;
Route::post('update_rating_user','API\RatingController@upadateRatingByUser');;

// insert
Route::post('update_rating_des','API\DestinationController@updatededes');

//blog
Route::post('show_blog','API\BlogController@index');
Route::post('show_hot_blog','API\BlogController@showHotMove');
Route::post('show_blog_by_category','API\BlogController@showBlogByCategory');

//event
Route::post('show_event_list','API\EventController@index');

//AgentTravel
Route::post('showAllAgent','API\AgentTravelController@showAllAgentTravel');

//Stores
Route::post('showAllStore','API\StoreController@showAllStore');
Route::post('showProdukByStoreId','API\StoreController@showProdukByStoreId');
Route::post('showNewStore','API\StoreController@showNewStore');
Route::post('showNewProduk','API\StoreController@showNewProduk');
//review store
Route::post('showReviewByStoreId','API\StoreController@showReviewByStoreId');

//covid19
Route::post('showCovidData','API\CovidController@covid19');
Route::get('showCovidINA','API\CovidController@showCovidINA');
Route::get('showCovidIdProvince','API\CovidController@showCovidIdProvince');

