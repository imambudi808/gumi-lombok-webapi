<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//

//start new root
//home page
Route::get('/','GumiHomeController@index')->name('gumi_home.index');
Route::get('/gumi-home','GumiHomeController@index')->name('gumi_home.index');
Route::get('/gumi-destinations','GumiHomeController@destinations')->name('gumi_destination.destinations');
Route::get('/gumi-blogs','GumiHomeController@blogs')->name('gumi_blog.blogs');
Route::get('/gumi-events','GumiHomeController@events')->name('gumi_event.events');
Route::get('/gumi-destination-detail/{seo_link}','GumiHomeController@destination_detail')->name('gumi_destination.detail');
Route::get('/gumi-blog-detail/{seo_link}','GumiHomeController@blog_detail')->name('gumi_blog.detail');
Route::get('/gumi-event-detail/{seo_link}','GumiHomeController@event_detail')->name('gumi_event.detail');
//end migrasi route

Route::get('/tentang-gumi-lombok', function () {
    return view('welcome');
})->name('gumi');




Route::get('/old_home', 'DepanController@index')->name('depan.index');
// Route::put('/detail-blog/{blog_id}','DepanController@detailBlog');
Route::get('/detail-blog/{blog_id}', 'DepanController@detailBlog')->name('depan.blog_detail');
Route::get('/detail-destinasi/{destination_id}','DepanController@detailDestinasi')->name('depan.destinasi_detail');
Route::get('/informasi-category/{blog_category}','DepanController@category')->name('depan.blog_category');
Route::get('/informasi-destinasi-wisata','DepanController@allDestinasi')->name('depan.semu_destinasi');

Route::get('/maps','DepanController@maps')->name('depan.maps');


// Route::resource('/order', 'OrderController');

Route::get('wisata','WisataController@index')->middleware('api');
Route::post('wisata','WisataController@insertWisata');
Route::put('/wisata/{id}','WisataController@updateWisata');

Route::delete('/wisata/{id}','WisataController@deleteWisata');

Route::get('/email','EmailController@showForm');
Route::post('/email','EmailController@sub');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/api_user','ApiController@showUser');
Route::get('/api_user_guzzle','ApiController@showUserGuzzle');

Route::get('/translate','ApiController@translate');
Route::post('/translate','ApiController@posttranslate');




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');


Route::group(['middleware' => 'auth'], function () {
	
	Route::get('table-list', function () {
		return view('pages.table_list');
	})->name('table');

	Route::get('typography', function () {
		return view('pages.typography');
	})->name('typography');

	Route::get('icons', function () {
		return view('pages.icons');
	})->name('icons');

	Route::get('map', function () {
		return view('pages.map');
	})->name('map');

	Route::get('notifications', function () {
		return view('pages.notifications');
	})->name('notifications');

	Route::get('rtl-support', function () {
		return view('pages.language');
	})->name('language');

	Route::get('upgrade', function () {
		return view('pages.upgrade');
	})->name('upgrade');
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);
});

Route::group(['middleware' => 'auth'],function(){
	Route::resource('destination', 'DestinationController',['except' => ['show']]);
});

Route::group(['middleware' => 'auth'],function(){
	Route::resource('point', 'PointController',['except' => ['show']]);
});

Route::group(['middleware' => 'auth'],function(){
	Route::resource('order', 'OrderController',['except' => ['show']]);
});
Route::group(['middleware' => 'auth'],function(){
	Route::resource('tiket', 'TiketController',['except' => ['show']]);
});

Route::group(['middleware' => 'auth'],function(){
	Route::resource('blog', 'BlogController',['except' => ['show']]);
});

Route::group(['middleware' => 'auth'],function(){
	Route::resource('event', 'EventController',['except' => ['show']]);
});




